#!/bin/bash
#MASTER SIMULATOR! 20171001
package=GTWAS_POOL_RADseq_SIM

#######################
### 				###
### IMPORTANT NOTES	###
###					###
#######################
#copy GTWAS_POOL_RADseq_SIM/ folder into /home/username/
#copy ddRADseqTools/ folder into /home/username/
#copy GWAlpha/ folder into /home/username/

###########################
###						###
### Arguments handling: ###
###						###
###########################
if [ $# == 0 ]; then
	echo "No arguments passed. Try again dude."
	echo " "
fi

while test $# -gt 0
do
	case "$1" in
		-h|--help)
			echo " "
			echo " "
			echo "$package - a suite of R, python, and Bash scripts as well as other Bioinformatics"
			echo "softwares for optimizing RADseq / GBS given a reference genome in fasta format"
			echo "Modify number of replications and othe RADseq parameters within this script."
			echo " "
			echo "Arguments:"
			echo "-h, --help		show brief help"
			echo "-g, --genome=GENOME	specify genome to use (full path to fasta)"
			echo "-a, --action=ACTION	specify the simulation to implement:"
			echo "			0 - for restriction enzyme and fragment size range optimization;"
			echo "			1 - for number of sequencing reads optimization"
			echo "-o, --output-dir=DIR	specify the directory where the output will be stored (full path)"
			echo " "
			echo "Examples:"
			echo "nohup ./GTWAS_POOL_RADseq_SIM.sh -g /home/ubuntu/SEQUENCES/DNA/lope_V1.0.fasta -a 0 -o /volume1/SIMULATED/DNA/ &"
			echo "nohup ./GTWAS_POOL_RADseq_SIM.sh -g /home/ubuntu/SEQUENCES/DNA/lope_V1.0.fasta -a 1 -o /volume1/SIMULATED/DNA/ &"
			echo " "
			echo " "
			exit 0
			;;
		-g)
			shift
			if test $# -gt 0; then
				export GENOME=$1
			else
				echo "no genome specified"
				exit 1
			fi
			shift
			;;
		--genome*)
			export GENOME=`echo $1 | sed -e 's/^[^=]*=//g'`
			shift
			;;
		-a)
			shift
			if test $# -gt 0; then
				export ACTION=$1
			else
				echo "no process specified"
				exit 1
			fi
			shift
			;;
		--action*)
			export ACTION=`echo $1 | sed -e 's/^[^=]*=//g'`
			shift
			;;
		-o)
			shift
			if test $# -gt 0; then
				export OUTPUT_DIR=$1
			else
				echo "no output directory specified"
				exit 1
			fi
			shift
			;;
		--output-dir*)
			export OUTPUT_DIR=`echo $1 | sed -e 's/^[^=]*=//g'`
			shift
			;;
			*)
		break
		;;
	esac
done
#gnildnah stnemugrA

########################################################################################################################
########################################################################################################################
########################################################################################################################

###########################################################################
###																		###
### O P T I M I Z E    R E S T R I C T I O N    E N Z Y M E    P A I R 	###
###				   A N D   F R A G M E N T   R A N G E 					###
###				F O R   R A D s e q   G E N O T Y P I N G				###
###																		###
###########################################################################

function restrictionEnzymePair_fragmentRange_simulation {

	#######################
	###				    ###
	### SETUP VARIABLES ###
	###					###
	#######################
	GENOME=$(echo $1 | rev | cut -d'/' -f1 | rev)
	GENOME_DIR=$(echo $1 | rev | cut -d'/' -f2- | rev)
	OUTPUT_DIR=$2

	NREP=5
	#MINFRAG=50
	#MAXFRAG=300
	INSERTLEN=75
	MUTPROB=0.0000001
	LOCNUM=200000
	READNUM=10000000
	KEEP_DEPTH=0

	########################
	###					 ###
	### MAIN OUTPUT FILE ###
	###					 ###
	########################
	echo -e "TEST\tENZYME_1\tENZYME_2\tMIN_FRAG\tMAX_FRAG\tINSERT_LEN\tNUM_READS\tTOTAL_BASES\tTOTAL_DEPTH\tTOTAL_SQUARED_DEPTH\tTOTAL_COVERAGE" > ${OUTPUT_DIR}/RADseq_SIMULATION.out

	###################################
	###								###
	### FIX REFERENCE GENOME FORMAT ###
	###								###
	###################################
	~/GTWAS_POOL_RADseq_SIM/fixREF.py $GENOME_DIR $GENOME Reference.fasta
	#INPUTS:
	#	(1) directory where the input fasta file to fix is (complete directory name!!!)
	#	(2)	input fasta file to fix
	#	(3) filename of the out_genesput fasta file
	#OUTPUT:
	#	Lolium_perenne_genome_FIXED.fasta --> each line truncated just right for bwa and bcftools!

	################################
	###							 ###
	### PREPARE REFERENCE GENOME ###
	###							 ###
	################################
	cd $GENOME_DIR
	bwa index -p Reference_genome -a bwtsw Reference.fasta
	samtools faidx Reference.fasta
	mv Reference.fasta.fai Reference_genome.fai
	cp ${GENOME_DIR}/Reference* $OUTPUT_DIR
	
	########################
	###					 ###
	### PREPARE BARCODES ### (in this case just a single barcode - since we're pooling)
	###					 ###
	########################
	echo -e "#RECORD FORMAT: individual_id;replicated_individual_id or NONE;population_id;index1_seq(5'->3')\n# For GWAlpha2 pool\nPOOLED;NONE;pool;GGTCTT;ATCACG" > ~/ddRADseqTools/Package/poolsIND.txt

	########################################
	###									 ###
	### PREPARE RESTRICTION ENZYMES LIST ###
	###									 ###
	########################################
	cd ~/ddRADseqTools/Package*/
	echo -e "MseI\nEcoRI\nHindIII\nSacI" > Renzyme.txt
	echo -e "AatII\nAcc65I\nAclI\nAflII\nAgeI\nApaI\nApaLI\nAscI\nAseI\nAsiSI" >> Renzyme.txt
	echo -e "AvrII\nBamHI\nBclI\nBglII\nBmtI\nBsiWI\nBspEI\nBspHI\nBsrGI\nBssHII" >> Renzyme.txt
	echo -e "BstBI\nClaI\nEagI\nFseI\nKpnI\nMfeI\nMluI\nNarI\nNcoI\nNdeI" >> Renzyme.txt
	echo -e "NgoMIV\nNheI\nNotI\nNsiI\nPacI\nPciI\nPspOMI\nPstI\nPvuI\nSacII" >> Renzyme.txt
	echo -e "SalI\nSbfI\nSpeI\nSphI\nXbaI\nXhoI\nXmaI" >> Renzyme.txt


	cd ~/GTWAS_POOL_RADseq_SIM
	for PREFIX in $(seq $NREP)
	do
		#ITERATE ACROSS DIFFERENT FRAGMENT SIZE RANGES: minimum size from 50-200 and maximum size from 250-400 (expecting [51*51/2]*[3]*[3] = 3,825 outputs per replicate)
		parallel ./simulateRADseq.sh $OUTPUT_DIR {1} {2} $INSERTLEN $MUTPROB $LOCNUM $READNUM $PREFIX $KEEP_DEPTH ::: $(seq 50 50 200) ::: $(seq 250 50 400)
		#INPUT:
		#	(1) the output directory containing the reference genome and its corresponding index files are located
		#	(2) minimum fragment size to select after RE digest but before sequencing
		#	(3) maximum fragment size to select after RE digest but before sequencing
		#	(4) sequence length of inserted DNA fragment
		#	(5) sequencing mutation probability in decimal form
		#	(6) number of loci to capture
		#	(7) number of rsequencing reads
		#	(8) prefix of output filename
		#	(9) keep depth file? 1 for yes and 0 for no
		#OUTPUT:
		#	(a) ${FILE_PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt --> RADseq stats
		cat ${OUTPUT_DIR}/${PREFIX}-* >> ${OUTPUT_DIR}/RADseq_SIMULATION.out
		rm ${OUTPUT_DIR}/${PREFIX}-*
	done
}


########################################################################################################################
########################################################################################################################
########################################################################################################################


###########################################################################
###																		###
### 		O P T I M I Z E    N U M B E R   O F    R E A D S , 	   	###
###					N U M B E R    O F    Q T L ,    A N D				###
###							H E R I T A B I L I T Y						###
###																		###
###########################################################################


function readsNumber_simulation {

	#######################
	###				    ###
	### SETUP VARIABLES ###
	###					###
	#######################
	GENOME=$(echo $1 | rev | cut -d'/' -f1 | rev)
	GENOME_DIR=$(echo $1 | rev | cut -d'/' -f2- | rev)
	OUTPUT_DIR=$2

	NREP=5
	MAX_NPOOL=10
	MAX_NQTL=100
	MAX_H2=100

	NVAR=20
	NGEN=500
	PREFIX=GENO

	MODEL=1

	ENZYME1=EcoRI
	ENZYME2=MseI
	MINFRAG=50
	MAXFRAG=300
	INSLEN=75
	MUTPROB=0.0000001
	LOCNUM=200000
	READNUM=10000000
	KEEP_DEPTH=1

	###################################
	###								###
	### FIX REFERENCE GENOME FORMAT ###
	###								###
	###################################
	cp ~/GTWAS_POOL_RADseq_SIM/VCF.header $OUTPUT_DIR
	~/GTWAS_POOL_RADseq_SIM/fixREF.py $GENOME_DIR $GENOME Reference.fasta
	#INPUTS:
	#	(1) directory where the input fasta file to fix is (complete directory name!!!)
	#	(2)	input fasta file to fix
	#	(3) filename of the out_genesput fasta file
	#OUTPUT:
	#	Lolium_perenne_genome_FIXED.fasta --> each line truncated just right for bwa and bcftools!

	################################
	###							 ###
	### PREPARE REFERENCE GENOME ###
	###							 ###
	################################
	cd $GENOME_DIR
	bwa index -p Reference_genome -a bwtsw Reference.fasta
	samtools faidx Reference.fasta
	mv Reference.fasta.fai Reference_genome.fai
	cp ${GENOME_DIR}/Reference* $OUTPUT_DIR
	#transcripts to follow.....

	########################
	###					 ###
	### PREPARE BARCODES ### (in this case just a single barcode - since we're pooling)
	###					 ###
	########################
	echo -e "#RECORD FORMAT: individual_id;replicated_individual_id or NONE;population_id;index1_seq(5'->3')\n# For GWAlpha2 pool\nPOOLED;NONE;pool;GGTCTT;ATCACG" > ~/ddRADseqTools/Package/poolsIND.txt

	# #for testing
	# rep=1
	# npool=5
	# nqtl=10
	# h2=50

	echo -e "REP\tNUMBER_OF_POOLS\tNUMBER_OF_QTL\tHERITABILITY\tENZYME1\tENZYME2\tMINFRAG\tMAXFRAG\tINSERTLEN\tAUC\tRMSD" > ${OUTPUT_DIR}/GTWAS_POOLED_SIMULATION.out

	#Iterate across replications, number of pools, number QTL and heritabilities (given present reps, pools, QTL and h2 parameters we expect 2,888 output files [RMSD+AUC] per replicate)
	for rep in $(seq 1 1 $NREP)
	do
		for npool in $(seq 3 1 $MAX_NPOOL)
		do
			for nqtl in $(seq 5 5 $MAX_NQTL)
			do
				for h2 in $(seq 10 5 $MAX_H2)
				do
					##############################
					## 	  SIMULATE GENOTYPES 	##
					## __simulateGenotypes__.sh ##
					##############################
					~/GTWAS_POOL_RADseq_SIM/simulateGenotypes.sh $OUTPUT_DIR $NVAR $NGEN $PREFIX N
					#INPUTS:
					#	(1) working directory where the refernce genome is located and where all outputs will be written into
					#	(2) number of variants (SNPS, deletions, substitutions and duplications) per scaffold to generate
					#	(3) number of individuals to simulate
					#	(4)	prefix of genotype names
					#	(5) assess LD? - Y for yes and N for no
					#OUTPUTS:
					#	(a) Simulated_Lolium_perenne_GENOTYPES.vcf
					#	(b) Simulated_Lolium_perenne_VARIANT.vcf ---> vcf file without genotypes - only contains the loci and allele states
					#	(c) Simulated_Lolium_perenne_GENOTYPES.data ---> loci across rows and individuals across columns, genotype names as header, includes scaffold names and positions
					#	(d) Simulated_Lolium_perenne_LOCI.data ---> SNP scaffold locations, positions, allele states, and reference allele frequences of all SNPs, headerless
					bgzip -c ${OUTPUT_DIR}/Genotypes.vcf > ${OUTPUT_DIR}/Genotypes.vcf.gz
					tabix -p vcf ${OUTPUT_DIR}/Genotypes.vcf.gz
					rm ${OUTPUT_DIR}/Genotypes.vcf

					##################################
					## 	  SIMULATE TRANSCRIPTOME	##
					##  __simulateTranscripts__.sh 	##
					##################################

					###############################
					## 	 SIMULATE PHENOTYPES 	 ##
					## __simulatePhenotypes__.sh ##
					############################### includes pooling
					~/GTWAS_POOL_RADseq_SIM/simulatePhenotypes.sh $OUTPUT_DIR Genotypes.data $nqtl $h2 $npool $MODEL
					#INPUTS:
					#	(1) working directory where the refernce genome is located and where all outputs will be written into
					#	(2) numeric genotype file ---> Simulated_Lolium_perenne_GENOTYPES.data
					#	(3) number of QTLs to simulate
					#	(4) heritability in percent whole number
					#	(5) number of pools to use to divide the individuals into
					#	(6) phenotype model to use
					#	(7) transcriptome file 1: base levels -- use if model > 1
					#	(8) transcriptome file 1: genotype-specific levels --- use if model > 1
					#OUTPUTS:
					#	(1) Simulated_Lolium_perenne_PHENOTYPES.data ---> headerless phenotypic values
					#	(2) Simulated_Lolium_perenne_PHENOTYPES_POOLED.index ---> index where each genotype belongs to which pool
					#	(3) Simulated_Lolium_perenne_PHENOTYPES.py ---> input phenotype stats for GWAlpha
					#	(4) Simulated_Lolium_perenne_QTL.data ---> the locations (scaffold and positions) and effects of simulated QTL
					#pooling individuals by phenotypes: [#worry not about the geno names in the vcf files - phenotyping and pooling counted them consecutively and does not correstpond to the vcf geno names]
					cd $OUTPUT_DIR
					cut -d$'\t' -f1,2 Phenotypes_pooled.index | sed 's/\.0//g' > INDEX.temp
					cut -d$'\t' -f3- Genotypes.data > GENOTYPE.temp
					for i in $(seq $npool)
					do
						awk -v VAR="$i" '{if ($1==VAR) print $2}' INDEX.temp | sed ':a;N;$!ba;s/\n/,/g' > POOL_US.temp
						cut -d$'\t' -f$(cat POOL_US.temp) GENOTYPE.temp > POOLED_GENO.temp
						~/GTWAS_POOL_RADseq_SIM/extractAlleleFreqPerPOOL.py $OUTPUT_DIR POOLED_GENO.temp
						#INPUTS:
						#	(1) working drectory: folder where the numeric genotype file is located and where the output will written on
						#	(2) numeric genotype data (rows:loci, columns:genotypes), header=GENOTYPES
						#OUTPUT:
						#	(a) FREQ.temp --> reference allele frequency per loci
						mv FREQ.temp FREQ-POOL-${i}.temp
					done
					tail -n+2 Genotypes.data | cut -d$'\t' -f1,2 > POS_NAMES.temp
					paste -d$'\t' POS_NAMES.temp FREQ-POOL-*.temp > Frequency.pools
					rm *.temp

					###########################
					## 	 SIMULATE RADseq 	 ## HOW TO REFLECT SNP ON CUT SITES??!!!!!!
					## __simulateRADseq__.sh ##  --> may not be possible without simulating whole genomes - which will take soOoOoOo long! 
					###########################	 --> maybe more practical to simulate numeric genotypes directly instead of whole genomes!!!
					cd ~/ddRADseqTools/Package*/
					echo ${ENZYME1} > Renzyme.txt
					echo ${ENZYME2} | cat >> Renzyme.txt
					~/GTWAS_POOL_RADseq_SIM/simulateRADseq.sh $OUTPUT_DIR $MINFRAG $MAXFRAG $INSLEN $MUTPROB $LOCNUM $READNUM $rep $KEEP_DEPTH
					#INPUT:
					#	(1) the output directory containing the reference genome and its corresponding index files are located
					#	(2) minimum fragment size to select after RE digest but before sequencing
					#	(3) maximum fragment size to select after RE digest but before sequencing
					#	(4) length of fragments for adaptor ligation and sequencing
					#	(5) sequencing mutation probability in decimal form
					#	(6) number of loci to capture
					#	(7) number of rsequencing reads
					#	(8) prefix of output filename
					#	(9) keep depth file? 1 for yes and 0 for no
					#OUTPUT:
					#	(a) ${FILE_PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt --> RADseq stats
					mv /${OUTPUT_DIR}/${rep}-${ENZYME1}x${ENZYME2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt /${OUTPUT_DIR}/${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.stats
					mv /${OUTPUT_DIR}/${rep}-${ENZYME1}x${ENZYME2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv /${OUTPUT_DIR}/DEPTH.temp
					mv /${OUTPUT_DIR}/${rep}-${ENZYME1}x${ENZYME2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-REDIGEST.csv /${OUTPUT_DIR}/${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.redigest

					#########################
					## 	 GENERATE SYNC 	   ##
					## __generateSYNC__.sh ## For each pool: given (1) allele frequences from __simulateGenotypes__.sh and (2) coverage from __simulateRADseq__.sh;
					######################### sample from binomial distribution the number of copies of each allele across all loci
					cd $OUTPUT_DIR
					awk -v OFS='\t' '{if ($3!=0) print $1,$2,$3}' DEPTH.temp > READS.temp1
					cut -d$'\t' -f1 READS.temp1 | uniq > scaffolds.temp1
					for scaf in $(cat scaffolds.temp1)
					do
						#echo $scaf
						grep $scaf READS.temp1 > RADseq_ALLPOS.temp
						grep $scaf Frequency.pools > R_INPUT_GENpool.temp
						grep $scaf Genotypes_LOCI.data > R_INPUT_LOCIdata.temp

						cut -d$'\t' -f2 R_INPUT_GENpool.temp > GENpoolPOS.temp
						touch R_INPUT_RADseq.temp
						for pos in $(cat GENpoolPOS.temp)
						do
							grep $'\t'$pos$'\t' RADseq_ALLPOS.temp >> R_INPUT_RADseq.temp
						done
						if [ $(cat R_INPUT_RADseq.temp | wc -l) -gt 0 ]
						then
							~/GTWAS_POOL_RADseq_SIM/generateSYNC.py $OUTPUT_DIR R_INPUT_RADseq.temp R_INPUT_GENpool.temp R_INPUT_LOCIdata.temp
							#INPUTS:
							#       (1) working drectory: folder where the numeric genotype file is located and where the output will written on
							#       (2) RADseq scaffold name, positions and coverage ---> R_INPUT_RADseq.temp
							#       (3) simulated variation scaffold name, positions and reference allele frequencies per pool ---> R_INPUT_GENpool.temp
							#       (4) simulated variation scaffold name, posisiotns, reference allele state and alternate allele state ---> R_INPUT_LOCIdata.temp
							#OUTPUT:
							#       (a) SYNC.temp ---> sync file: scaffold name, position, reference allele state, counts of A:C:T:G:N:DEL per pool (a column per pool)
							#		(b) RMSD_freq_ORIG.temp ---> originally simulated reference allele frequencies (to be compared later with the sync file derived allele frequencies through root mean squared deviation [RMSD] metric)
							mv SYNC.temp SCAFFOLD-${scaf}-OUT.sync
							mv RMSD_freq_ORIG.temp RMSD_freq_ORIG-${scaf}.out
						fi
						rm *.temp
					done
					cat SCAFFOLD* > RADseq.sync
					cat *ORIG* > RMSD_freq_ORIG.temp
					rm SCAFFOLD* *ORIG*.out
					rm *.temp1

					# #extracting allele frequencies from the sync file and comparing it with the originally simulated allele frequencies
					cut -d$'\t' -f4- RADseq.sync | cut -d$'\t' -f4- RADseq.sync | sed 's/:0:0:0:0//g' | sed 's/:0:0:0//g' | sed 's/:0:0//g' | sed 's/:0//g' | sed 's/^0://g' | sed 's/\t0:/\t/g' | sed 's/:/+/g' > RMSD_counts.temp
					for p in $(seq $npool)
					do
						cut -d$'\t' -f$p RMSD_counts.temp > RMSD_counts_per_POOL.temp
						cat RMSD_counts_per_POOL.temp | bc > RMSD_sums_per_POOL.temp
						cut -d+ -f1 RMSD_counts_per_POOL.temp > RMSD_allele1_per_POOL.temp
						#paste -d/ RMSD_allele1_per_POOL.temp RMSD_sums_per_POOL.temp | bc >> RMSD_freq_SYNC.temp
						R -q -e 'x=read.delim("RMSD_allele1_per_POOL.temp", header=F); y=read.delim("RMSD_sums_per_POOL.temp", header=F); write.table(x$V1/y$V1, file="freq_SYNC.temp", row.names=F, col.names=F)'
						mv freq_SYNC.temp freq_SYNC-${p}.temp
					done
					paste -d$'\t' freq_SYNC-*.temp > RMSD_freq_SYNC.temp
					#due to the computational inefficiency (at least given my current abilities) to extract REF and ALT orig and syn allele frequencies I just used pq = p(1-p) = p-p^2 instead of just p or just q
					R -q -e 'x=read.delim("RMSD_freq_ORIG.temp", header=F); y=read.delim("RMSD_freq_SYNC.temp", header=F); x1 = x - x^2; y1 = y - y^2; write.table(mean(sqrt(colMeans((x1-y1)^2))), file="RMSD.temp", row.names=F, col.names=F)'
					RMSD=$(cat RMSD.temp)
					rm *.temp
					#PROBLEM!!! HOW TO MATCH REF ALLELE IN SYNC WITH REF IN ORIG. SIM?!!! ==>>> SOLUTION MULTIPLY THEM 2 ALLELE FREQS! PROD = p(1-p) = p - p^2
					#cat RMSD_counts.temp | bc > RMSD_means.temp

					##########################
					## ASSOSIATION ANALYSIS ##
					##		GWAlpha.py 		##
					##########################
					mv RADseq.sync ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.sync
					mv Phenotypes.py ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_pheno.py
					~/GWAlpha/GWAlpha.py ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.sync ML
					#INPUTS:
					#	(1) sync file of genotypes with the same name as the phenotype file in phyton format with the suffix "_pheno.py"
					#	(2) paramater estimation method to use LS for least squares or V(e) minimization and ML for maximum likelihood or maximizxation of P(parameters | data) ===> argmax(Prob.DATA as a function of parameters)
					#OUTPUT:
					#	(a) GWAlpha-{sync filenames}_out.csv ---> identitites of each loci and their corresponding alpha, MAF and COV values | higher alpha^2 the more significant the loci
					~/GTWAS_POOL_RADseq_SIM/GWAlphaPlot-jefdited.r GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_out.csv pval qqplot
					#INPUTS:
					#	(1) GWAlphay.py output --> GWAlpha-{sync filenames}_out.csv
					#	(2) metric to use: alpha or pval
					#	(3) plot qqlpot
					#OUTPUTS:
					#	(a) GWAlpha-{sync filenames}.png ---> manhattan plot
					#	(b) GWAlpha-{sync filenames}_qqplot.png ---> QQ-plot optional
					~/GTWAS_POOL_RADseq_SIM/plotROC.py $OUTPUT_DIR  GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_out.csv QTL.data 1000
					#INPUTS:
					#	(1) working directory where the GWAlpha.py output is found
					#	(2) GWAlphay.py output --> GWAlpha-{sync filenames}_out.csv
					#	(3) locations (scaffold and positions) and effects of simulated QTL ---> Simulated_Lolium_perenne_QTL.data
					#OUTPUT:
					#	(a) ROC.png ---> receiver operating characteristing with AUC metric
					mv GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_out.csv ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_GWAlpha.csv
					mv GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.png ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_manhattan.png
					mv GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_qqplot.png ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_qqplot.png
					mv ROC.png ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_ROC.png
					AUC=$(sed 's/ //g' ROC_AUC.temp)
					echo -e "$rep\t$npool\t$nqtl\t$h2\t$ENZYME1\t$ENZYME2\t$MINFRAG\t$MAXFRAG\t$INSLEN\t$AUC\t$RMSD" > ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_AUC_RMSD.out
					rm GWAlpha* ROC_AUC.temp
				done
			done
			cat ${OUTPUT_DIR}/*_AUC_RMSD.out >> ${OUTPUT_DIR}/GTWAS_POOLED_SIMULATION.out
			rm ${OUTPUT_DIR}/*_AUC_RMSD.out
		done
	done
}



########################################################################################################################
########################################################################################################################
########################################################################################################################

# IMPLEMENT!

########################################################################################################################
########################################################################################################################
########################################################################################################################

# #for testing:
# GENOME1=/home/ubuntu/SEQUENCES/DNA/lope_V1.0.fasta
# OUTPUT_DIR=/mnt/SIMULATED/DNA/

if [ $ACTION == 0 ]
	then
	echo "RE pairs and fragment size range RADseq simulation!"
	# Run the RADseq RE pair and fragment size range optimization simulation:
	cd ~/GTWAS_POOL_RADseq_SIM
	restrictionEnzymePair_fragmentRange_simulation $GENOME $OUTPUT_DIR

elif [ $ACTION == 1 ]
	then
	echo "Number of reads RADseq simulation plus GWAlpha!"
	# Prepare the scripts and output directories:
	cd ~/GTWAS_POOL_RADseq_SIM
	chmod +x *.py; chmod +x *.sh; chmod +x *.r
	# Run the RADseq number of reads optimization simulation:
	readsNumber_simulation $GENOME $OUTPUT_DIR
else
	echo "WTF! That's not a valid action!"
fi

########################################################################################################################
########################################################################################################################
########################################################################################################################
# !TNEMELPMI
########################################################################################################################
########################################################################################################################
########################################################################################################################