#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
work_DIR = args[1]
freqFile = args[2]
LDFile = args[3]

setwd(work_DIR)

freq = read.delim(freqFile, sep="\t", header=TRUE)
LD = read.delim(LDFile, sep="\t", header=TRUE, row.names=NULL)

distance = abs(LD[,3] - LD[,2])
R2 = LD[,5]

FREQ = apply(matrix(freq[,1], nrow(freq), ncol=1), 1, function(x) if(x>0.5) {x-0.5} else {x})

plot(density(FREQ))

plot(R2 ~ distance)

jpeg(paste(LDFile, ".ALLELE_FREQ_PLOT_FOLDED.jpeg", sep=""))
plot(density(FREQ))
dev.off()

jpeg(paste(LDFile, ".ALLELE_FREQ_PLOT_UNFLURLILING.jpeg", sep=""))
plot(density(freq[,1]))
dev.off()

jpeg(paste(LDFile, ".LD_PLOT.jpeg", sep=""))
plot(R2 ~ distance)
dev.off()
