#!/bin/bash
# GLM - no pre-processing from fasta: directly from simulated vcf*
##	generate Xb+e vs Xb
./basisPlots_GWAS_ROC.py $1 $2 $3 $4 $5
# #	generate the manhattan plot
# ./GWAS_individuals.R $1 $2 $3 1
# #	ROC plotting
# cd $1
# grep "scaffold" $8 | cut -d$'\t' -f1,2,4 > Simulated_Lolium_perenne_GWAS_FULL_GLM.temp
# echo $newline > Simulated_Lolium_perenne_GWAS_FULL_GLM.lod
# paste -d $'\t' Simulated_Lolium_perenne_GWAS_FULL_GLM.temp GWAS_GLM_LOD.temp >> Simulated_Lolium_perenne_GWAS_FULL_GLM_2.temp
# sed 's/\t/,/g' Simulated_Lolium_perenne_GWAS_FULL_GLM_2.temp > Simulated_Lolium_perenne_GWAS_FULL_GLM.lod
# #rm *.temp
# ~/SCRIPTS/plotROC.py $1  Simulated_Lolium_perenne_GWAS_FULL_GLM.lod Simulated_Lolium_perenne_QTL.data 1000
# mv ROC.png ALL_LOCI\ ROC\ \-\ GLM.png

#GWAlpha - pre-processing: vcf*->fasta->bam->mpileup->sync
cd $1
mem=$(($(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1000000))-5))
thr=$(($(grep -c ^processor /proc/cpuinfo)-1))
npools=5

#for testing:
#$7 --> Simulated_Lolium_perenne_PHENOTYPES_POOLED.index
#$8 --> Simulated_Lolium_perenne_GENOTYPES.vcf

#group the genotypes per pool - generate 5 vcf files
grep "scaffold" $8 | cut -d$'\t' -f10- > vcf.temp
grep "scaffold" $8 | cut -d$'\t' -f1-9 > vcf2.temp
for i in $(seq $npools)
do
npool=$(cut -d$'\t' -f1 $7 | grep ${i} | wc -w)
nTOT=$(($i * $npool))
FIELDS=$(head -n$(($nTOT +1)) $7 | tail -n $npool | cut -d$'\t' -f2 | sed ':a;N;$!ba;s/\n/,/g' | sed 's/\.0//g')
cut -d$'\t' -f$FIELDS vcf.temp > vcf.pool.temp1
paste vcf2.temp vcf.pool.temp1 > vcf.pool.temp2
cat VCF.header vcf.pool.temp2 > Simulated_Lolium_perenne_GENOTYPES_POOL${i}.vcf
done

#genetaing the sync files
parallel "~/SCRIPTS/vcf2sync.py Simulated_Lolium_perenne_GENOTYPES_POOL{1}.vcf" ::: $(seq $npools)
#merging pools into 1 sync file
cut -d$'\t' -f1-3 Simulated_Lolium_perenne_GENOTYPES_POOL1.sync > Simulated_Lolium_perenne_GENOTYPES_POOLED.sync
for i in $(seq $npools)
do
cut -d$'\t' -f4 Simulated_Lolium_perenne_GENOTYPES_POOL${i}.sync > temp.txt
paste -d$'\t' Simulated_Lolium_perenne_GENOTYPES_POOLED.sync temp.txt > temp2.txt
rm Simulated_Lolium_perenne_GENOTYPES_POOLED.sync
mv temp2.txt Simulated_Lolium_perenne_GENOTYPES_POOLED.sync
done

cp Simulated_Lolium_perenne_PHENOTYPES.py sim-ALL_LOCI_pheno.py
cp Simulated_Lolium_perenne_GENOTYPES_POOLED.sync sim-ALL_LOCI.sync
~/SOFTWARES/GWAlpha/GWAlpha.py sim-ALL_LOCI.sync ML
~/SCRIPTS/GWAlphaPlot-jefdited.r GWAlpha_sim-ALL_LOCI_out.csv pval qqplot
~/SCRIPTS/plotROC.py $1  GWAlpha_sim-ALL_LOCI_out.csv Simulated_Lolium_perenne_QTL.data 1000
mv ROC.png ALL_LOCI\ ROC\ \-\ GWAlpha.png

#test "bam mem" possible issue: multiple copies of sequence names are discarded
# for i in $(seq $nPools)
# do
# #test1
# bwa mem -t $thr -M Lolium_perenne_genome Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap1.fasta.gz Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap1.fasta.gz | samtools view -@ $thr -h -b -S | samtools sort -@ $thr -m ${mem}G > Simulated_Lolium_perenne_GENOTYPES-POOL${i}.bam
# bwa mem -t $thr -M Lolium_perenne_genome Simulated_Lolium_perenne_GENOTYPES-POOL${i}.fasta.gz | samtools view -@ $thr -h -b -S | samtools sort -@ $thr -m ${mem}G > Simulated_Lolium_perenne_GENOTYPES-POOL${i}.bam
# java -ea -Xmx${mem}g -jar ~/SOFTWARES/picard.jar BuildBamIndex I=Simulated_Lolium_perenne_GENOTYPES-POOL${i}.bam
#test2
# bwa mem -t $thr -M Lolium_perenne_genome \
# Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap1.fasta.gz \
# Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap2.fasta.gz \
# > Simulated_Lolium_perenne_GENOTYPES-POOL${i}-ALIGNED.sam
# #compress the aligned sam file into a bam file
# samtools view -@ $thr -Sb \
# Simulated_Lolium_perenne_GENOTYPES-POOL${i}-ALIGNED.sam \
# > Simulated_Lolium_perenne_GENOTYPES-POOL${i}-ALIGNED.bam
# #sort these bam files for indexing
# samtools sort -@ $thr -m ${mem}G Simulated_Lolium_perenne_GENOTYPES-POOL${i}-ALIGNED.bam \
# > Simulated_Lolium_perenne_GENOTYPES-POOL${i}-ALIGNED.bam.sorted
# mv Simulated_Lolium_perenne_GENOTYPES-POOL${i}-ALIGNED.bam.sorted Simulated_Lolium_perenne_GENOTYPES-POOL${i}-ALIGNED.bam
# #build bam index, #remove duplicates and #rebuild cleaned bam index with picard
# java -ea -Xmx${mem}g -jar ~/SOFTWARES/picard.jar BuildBamIndex \
# I=Simulated_Lolium_perenne_GENOTYPES-POOL${i}-ALIGNED.bam
# done
# #generate the .sync genotype file for GWAlpha
# ls Simulated_Lolium_perenne_GENOTYPES-POOL*.bam > bam.list
# samtools mpileup -b bam.list > Simulated_Lolium_perenne_GENOTYPES_POOLED.mpileup
# java -ea -Xmx${mem}g -jar ~/SOFTWARES/popoolation2*/mpileup2sync.jar \
# --input Simulated_Lolium_perenne_GENOTYPES_POOLED.mpileup \
# --output Simulated_Lolium_perenne_GENOTYPES_POOLED.sync \
# --fastq-type sanger \
# --min-qual 30

# cp Simulated_Lolium_perenne_PHENOTYPES.py sim-ALL_LOCI_pheno.py
# cp Simulated_Lolium_perenne_GENOTYPES_POOLED.sync sim-ALL_LOCI.sync
# ~/SOFTWARES/GWAlpha/GWAlpha.py sim-ALL_LOCI.sync ML
# ~/SCRIPTS/GWAlphaPlot-jefdited.r GWAlpha_sim-ALL_LOCI_out.csv pval qqplot
# ~/SCRIPTS/plotROC.py $1  GWAlpha_sim-ALL_LOCI_out2_PVALUES.csv Simulated_Lolium_perenne_QTL.data 1000
# mv RADseq-pooled\ ROC.png ALL_LOCI\ ROC\ \-\ GWAlpha.png

# ######################################## TESTING
# #GLM - similar pre-processing as GWAlpha: vcf*->fasta->bam->vcf->numeric
# cd ${1}/RAW_SIMULATED_FASTA/
# mem=$(($(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1000000))-5))
# thr=$(($(grep -c ^processor /proc/cpuinfo)-1))
# nIND=$(cat ../Simulated_Lolium_perenne_PHENOTYPES.data | wc -l)
# # test 1:
# parallel "bwa mem -t $thr -M ../Lolium_perenne_genome Simulated_Lolium_perenne-${6}{1}-hap1.fasta.gz Simulated_Lolium_perenne-${6}{1}-hap2.fasta.gz | samtools view -@ $thr -Sb | samtools sort -@ $thr -m ${mem}G > Simulated_Lolium_perenne-${6}{1}.bam" ::: $(seq $nIND)
# parallel "java -ea -Xmx${mem}g -jar ~/SOFTWARES/picard.jar BuildBamIndex I=Simulated_Lolium_perenne-${6}{1}.bam" ::: $(seq $nIND)
# bcftools mpileup --threads $thr -Ob -f ../Lolium_perenne_genome_FIXED.fasta Simulated_Lolium_perenne-${6}*.bam | bcftools call -Ov -mv -o Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf
# #test2:
# cp ../Lolium_perenne_genome_FIXED.fasta ../Lolium_perenne_genome.fasta
# parallel "bwa mem -t $thr -M ../Lolium_perenne_genome Simulated_Lolium_perenne-${6}{1}-hap1.fasta.gz Simulated_Lolium_perenne-${6}{1}-hap2.fasta.gz > Simulated_Lolium_perenne-${6}{1}.bam" ::: $(seq $nIND)
# samtools mpileup -ov Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf -f ../Lolium_perenne_genome.fasta Simulated_Lolium_perenne-${6}*.bam
# bcftools call -vmO z -o <study.vcf.gz> <study.bcf>

# #test3:
# #test code below:
# bwa index ../Lolium_perenne_genome_FIXED.fasta
# genPREFIX=$6
# parallel "bwa mem -t $thr -M ../Lolium_perenne_genome_FIXED.fasta \
# Simulated_Lolium_perenne-${genPREFIX}{1}-hap1.fasta.gz \
# Simulated_Lolium_perenne-${genPREFIX}{1}-hap2.fasta.gz \
# > Simulated_Lolium_perenne-${genPREFIX}{1}-ALIGNED.sam" ::: $(seq $nIND)
# #compress the aligned sam file into a bam file
# parallel "samtools view -@ $thr -Sb \
# Simulated_Lolium_perenne-${genPREFIX}{1}-ALIGNED.sam \
# > Simulated_Lolium_perenne-${genPREFIX}{1}-ALIGNED-UNSORTED.bam" ::: $(seq $nIND)
# rm Simulated_Lolium_perenne-${genPREFIX}*-ALIGNED.sam
# #sort these bam files for indexing
# parallel "samtools sort -@ $thr -m ${mem}G Simulated_Lolium_perenne-${genPREFIX}{1}-ALIGNED-UNSORTED.bam \
# > Simulated_Lolium_perenne-${genPREFIX}{1}-ALIGNED.bam" ::: $(seq $nIND)
# rm Simulated_Lolium_perenne-${genPREFIX}*-ALIGNED-UNSORTED.bam
# #build bam index, #remove duplicates and #rebuild cleaned bam index with picard
# parallel "java -ea -Xmx${mem}g -jar ~/SOFTWARES/picard.jar BuildBamIndex \
# I=Simulated_Lolium_perenne-${genPREFIX}{1}-ALIGNED.bam" ::: $(seq $nIND)
# #variant calling
# ls Simulated_Lolium_perenne-${genPREFIX}*-ALIGNED.bam > bam.list
# bcftools mpileup --threads $thr -Ob -f ../Lolium_perenne_genome_FIXED.fasta -b bam.list | bcftools call -Ov -mv -o Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf
# #test code above:

# LEN=$(cat Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf | wc -l)
# tail -n$(($LEN-42)) Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf > test_OUT1.txt
# cut -d$'\t' -f10- test_OUT1.txt > test_OUT2.txt
# sed 's/[\t]/:/g' test_OUT2.txt > test_OUT3.txt
# nCOL=$(($(head -n1 test_OUT3.txt | grep -o ":" | wc -l)+1))
# echo $(seq 1 2 $nCOL) | sed 's/ /,/g' > test_OUT3.5.txt
# cut -d: -f$(cat test_OUT3.5.txt) test_OUT3.txt > test_OUT4.txt
# sed 's/:/\t/g' test_OUT4.txt > test_OUT5.txt
# sed 's/0\/0/2/g' test_OUT5.txt | sed 's/1\/1/0/g' | sed 's/1\/0/1/g' | sed 's/0\/1/1/g' > test_OUT6.txt

# tail -n$(($LEN-42)) Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf | cut -d$'\t' -f1,2 > LOCI_scaff_pos.txt


#for testing GWAS - reconstituted vcf from concensus fasta.py
# workDIR = "/mnt/SIMULATED/DNA/"
# phenotypeFile = "../PHE/Simulated_Lolium_perenne_PHENOTYPES.data"
# genotypeFile = "RAW_SIMULATED_FASTA/test_OUT6.txt"
# qtlFile = "Simulated_Lolium_perenne_QTL.data"
# phenModel = 0
# reconVCF = "RAW_SIMULATED_FASTA/Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf"

# ./GWAS\ \-\ reconstituted\ vcf\ from\ concensus\ fasta.py /${VOLUME}/SIMULATED/DNA \
# Simulated_Lolium_perenne_PHENOTYPES.data \
# Simulated_Lolium_perenne_GENOTYPES.numeric \
# Simulated_Lolium_perenne_QTL.data \
# 1 \
# /RAW_SIMULATED_FASTA/Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf

# PREF=Simulated_Lolium_perenne-GENO

# parallel "bwa mem -R '@RG\tID:group{1}' \
# test_Lolium_perenne_genome_FIXED.fasta \
# ${PREF}{1}-hap2.fasta.gz ${PREF}{1}-hap1.fasta.gz \
# > ${PREF}{1}.sam" ::: {1..500}


# parallel "samtools view -Sb ${PREF}{1}.sam | samtools sort > ${PREF}{1}.bam" ::: {1..500}
# parallel "samtools index ${PREF}{1}.bam" ::: {1..500}

# ls ${PREF}*.bam > bam.list
# samtools merge -b bam.list Simulated_Lolium_perenne-MERGED.bam

# bcftools mpileup -Ob \
#  -f test_Lolium_perenne_genome_FIXED.fasta \
# -b bam.list | bcftools call -Ov -mv \
# -o Simulated_Lolium_perenne_GENOTYPES-reconstructedFromFASTA.vcf

# bcftools mpileup -Ou -f test_Lolium_perenne_genome_FIXED.fasta Simulated_Lolium_perenne-MERGED.bam | \
# bcftools call -Ou -mv > test.vcf