# GTWAS_POOL_RADseq_SIM
A suite of Python, R and Bash scripts for simulating RADseq/GBS for pooled GWAS, TWAS and GTWAS

## Software Dependencies (Lots of them!):
```
sudo apt install -y make libncurses5-dev libncursesw5-dev python3-dev python-dev libbz2-dev liblzma-dev python3-numpy python-numpy unzip git make default-jre build-essential zlib1g-dev bwa vcftools r-base htop python-pip python3-pip python-tk parallel libboost-all-dev bedtools
pip install numpy scipy matplotlib Biopython statsmodels pandas sklearn pymp-pypi pysam; pip install pysamstats; pip install opencv-python scikit-image
wget http://zzz.bwh.harvard.edu/plink/dist/plink-1.07-x86_64.zip
wget https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/current/sratoolkit.current-ubuntu64.tar.gz
wget -O ddRADseqTools.zip https://github.com/GGFHF/ddRADseqTools/archive/master.zip
git clone git://github.com/samtools/samtools.git
git clone git://github.com/samtools/htslib.git
git clone git://github.com/samtools/bcftools.git
git clone https://github.com/aflevel/GWAlpha.git
wget -O cufflinks.tar.gz http://cole-trapnell-lab.github.io/cufflinks/assets/downloads/cufflinks-2.2.1.Linux_x86_64.tar.gz
git clone https://github.com/alexdobin/STAR.git
```
Move (or clone) ddRADseqTools and GWAlpha folders into /home/ubuntu/ directories - or modify the GTWAS_POOL_RADseq_SIM.sh script to point to the proper directories where GWAlpha and ddRADseqTools are located

## Usage:
```
chmod +x *.sh
./GTWAS_POOL_RADseq_SIM --help
```
* **Passing simulation arguments**
```
nohup ./GTWAS_POOL_RADseq_SIM -g /home/ubuntu/SEQUENCES/DNA/lope_V1.0.fasta -o /mnt/SIMULATED/DNA/ -a 0 &
```
	* [Arguments:]
		* [-g or --genome=] - reference genome in fasta format
		* [-o or --output=] - output directory where all output files will be written into (need not exist before running the script)
		* [-a or --action=] - simulation to perform: 0 for RE pair and insert length optimization and 1 for reads, pool, and inference model optimization


<!-- ## On using git (simple guide for miself):
```
cd $working_directory
git clone https://github.com/jeffersonfparil/GTWAS_POOL_RADseq_SIM.git
git add -A
git commit -am "message of addition, modfications and deletion of files" #commit to local repository
git push origin master #push local master repository into the origin of the git repository cloned : enter git username and password
``` -->

# Citing these magnificent dependencies

- Mora-Márquez F, García-Olivares V, Emerson BC, López de Heredia U. ddradseqtools : a software package for in silico simulation and testing of double-digest RADseq experiments. Molecular Ecology Resources. 2017;17: 230–246. doi:10.1111/1755-0998.12550
- Li H. Aligning sequence reads, clone sequences and assembly contigs with BWA-MEM. 2013; Available:http://arxiv.org/abs/1303.3997
- Li H. A statistical framework for SNP calling, mutation discovery, association mapping and population genetical parameter estimation from sequencing data. Bioinformatics. 2011;27: 2987–2993. doi:10.1093/bioinformatics/btr509
- Kofler R, Pandey RV, Schlötterer C. PoPoolation2: identifying differentiation between populations using sequencing of pooled DNA samples (Pool-Seq). Bioinformatics. Oxford University Press; 2011;27: 3435–3436. doi:10.1093/bioinformatics/btr589