#!/bin/bash
#		(1) directory where Lolium_perenne_genome_FIXED.fasta is located
#		(2) number of variants per scaffold to simulate
#		(3) number of genotypes to simulate
#		(4) prefix of the genotype names
#		(5)	LD stat?
DIR=$1
NVAR=$2
NGEN=$3
PREFIX=$4
LD=$5

# #for testing
# DIR=/mnt/SIMULATED/DNA
# NVAR=20
# NGEN=500
# PREFIX=GENO
# LD=N

#(1) Simulate one vcf file with all the possible variants
~/GTWAS_POOL_RADseq_SIM/simulateVAR.py $DIR \
Reference.fasta \
Variant.fasta \
Variant.vcf \
$NVAR

#(2) Simulate a number of genotypes and stack them all together in one vcf file
cd $DIR
nLOCI=$(grep "scaffold" ${DIR}/Variant.vcf | wc -l)

~/GTWAS_POOL_RADseq_SIM/simulateGEN_I.py $DIR \
${nLOCI} \
$NVAR \
$NGEN \
$PREFIX \
Variant.vcf

parallel ~/GTWAS_POOL_RADseq_SIM/simulateGEN_II.py $DIR phase.txt $NVAR Genotypes.freq ${PREFIX}{1} ::: $(seq $NGEN)

#(3) Finishing up
#concatenate genotypes
cd $DIR
paste -d$'\t' GENO* >> Genotypes.vcf.temp
echo ' ' > newline
cat newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline Genotypes.vcf.temp > binGEN.temp
paste -d$'\t' Variant.vcf binGEN.temp > Genotypes.vcf
rm GENO* *.temp newline phase.txt
#extract basic vcf file statistics including LD in R^2
if [ $LD == Y ]
then
vcftools --vcf Genotypes.vcf
vcftools --vcf Genotypes.vcf --freq --out Genotypes.vcfTOOLS
if [ $nLOCI -gt 10000 ]
then
head -n10000 Genotypes.vcf > forLD_extract.vcf
else
head -n$((19 + $nLOCI)) Genotypes.vcf > forLD_extract.vcf
fi
vcftools --vcf forLD_extract.vcf --hap-r2 --out Genotypes_PHASED.vcfTOOLS
cut -d$'\t' -f5 Genotypes.vcfTOOLS.frq |cut -d: -f2 > allele.freq
#plotting LD and allele frequencies
~/GTWAS_POOL_RADseq_SIM/plotLDandAlleleFreq.R $1 allele.freq Genotypes_PHASED.vcfTOOLS.hap.ld
rm allele.freq forLD_extract.vcf
fi
#extracting numeric data
grep "scaffold" Genotypes.vcf | cut -d$'\t' -f1,2,4,5 > Genotypes_LOCI.temp
grep "scaffold" Genotypes.vcf | cut -d$'\t' -f1,2 > geno.loci
grep "scaffold" Genotypes.vcf | cut -d$'\t' -f10- > geno.data
paste -d$'\t' geno.loci geno.data > geno.txt
sed 's/0|0/2/g' geno.txt | sed 's/1|1/0/g' | sed 's/1|0/1/g' | sed 's/0|1/1/g' >> geno.num
#extracting headers or genotype names
grep "#CHROM" Genotypes.vcf | cut -d$'\t' -f8- > header.data
sed 's/INFO/LOCI/g' header.data | sed 's/FORMAT/POS/g'> header.txt
#pineapple - apple uhhmmmm!
cat header.txt geno.num > Genotypes.data
#extract binary genotype data prior assigning phenotypic values
#cut -d$'\t' -f3- Genotypes.data > Genotypes.numeric
#summarize loci data
cut -d$'\t' -f2 Genotypes.freq | paste -d$'\t' Genotypes_LOCI.temp - > Genotypes_LOCI.data
rm Genotypes.freq Genotypes_LOCI.temp
#clean up
rm geno*
rm header*
#gzipping the vcf file of all genotypes
# cd $DIR
# bgzip -c Genotypes.vcf > Genotypes.vcf.gz
# tabix -p vcf Genotypes.vcf.gz
