#!/bin/bash
#MASTER SIMULATOR! 20170926

##############################
###						   ###
### SETUP GLOBAL VARIABLES ###
###						   ###
##############################
VOLUME=mnt #for newNECTAR and sim03
#VOLUME=volume1 #for sim02
NREP=5
NPOOL=10
NQTL=100
H2=100

NVAR=20
NGEN=500

MODEL=1

ENZYME1=EcoRI
ENZYME2=MseI
MINFRAG=100
MAXFRAG=800
FRAGINT=25
INSLEN=300
MUTPROB=0.0000001
LOCNUM=200000
READNUM=10000000
PREFIX=GENO
KEEP_DEPTH=1

###########################
###						###
### PREPARE THE SCRIPTS ###
###						###
###########################
cd ~/SCRIPTS
chmod +x *.py; chmod +x *.sh; chmod +x *.R

################################
###							 ###
### PREPARE REFERENCE GENOME ###
###							 ###
################################
~/SCRIPTS/fixREF.py /home/ubuntu/SEQUENCES/DNA/ lope_V1.0.fasta Lolium_perenne_genome_FIXED.fasta
	#INPUTS:
	#	(1) directory where the input fasta file to fix is (complete directory name!!!)
	#	(2)	input fasta file to fix
	#	(3) filename of the out_genesput fasta file
	#OUTPUT:
	#	Lolium_perenne_genome_FIXED.fasta --> each line truncated just right for bwa and bcftools!
cd ~/SEQUENCES/DNA/
bwa index -p Lolium_perenne_genome -a bwtsw Lolium_perenne_genome_FIXED.fasta
samtools faidx Lolium_perenne_genome_FIXED.fasta
mv Lolium_perenne_genome_FIXED.fasta.fai Lolium_perenne_genome.fai
cp ~/SEQUENCES/DNA/Lolium_perenne_genome* /${VOLUME}/SIMULATED/DNA/
# cp ~/SEQUENCES/RNA/lope_transcripts.V1.0.fasta /${VOLUME}/SIMULATED/RNA/Lolium_perenne_transcriptome.fasta
# cp ~/SEQUENCES/RNA/Lp_Annotation_V1.1.mp.gff3 /${VOLUME}/SIMULATED/RNA/Lolium_perenne_annotations.gff3
cp ~/SCRIPTS/VCF.header /${VOLUME}/SIMULATED/DNA


#########################
###				      ###
###    ITERATE OVER   ###
###	 NUMBER OF POOLS  ###
###		NUMBER OF QTL ###
###			 &	   	  ###
###	 HERTITABILITIES  ###
###				      ###
#########################
# #for testing
# rep=1
# npool=5
# nqtl=2
# h2=50

for rep in $(seq 1 1 $NREP)
do
for npool in $(seq 2 1 $NPOOL)
do
for nqtl in $(seq 2 2 $NQTL)
do
for h2 in $(seq 10 10 $H2)
do
##############################
## 	  SIMULATE GENOTYPES 	##
## __simulateGenotypes__.sh ##
##############################
~/SCRIPTS/__simulateGenotypes__.sh /${VOLUME}/SIMULATED/DNA $NVAR $NGEN $PREFIX N
#INPUTS:
#	(1) working directory where the refernce genome is located and where all outputs will be written into
#	(2) number of variants (SNPS, deletions, substitutions and duplications) per scaffold to generate
#	(3) number of individuals to simulate
#	(4)	prefix of genotype names
#	(5) assess LD? - Y for yes and N for no
#OUTPUTS:
#	(a) Simulated_Lolium_perenne_GENOTYPES.vcf
#	(b) Simulated_Lolium_perenne_VARIANT.vcf ---> vcf file without genotypes - only contains the loci and allele states
#	(c) Simulated_Lolium_perenne_GENOTYPES.data ---> loci across rows and individuals across columns, genotype names as header, includes scaffold names and positions
#	(d) Simulated_Lolium_perenne_LOCI.data ---> SNP scaffold locations, positions, allele states, and reference allele frequences of all SNPs, headerless
bgzip -c /${VOLUME}/SIMULATED/DNA/Simulated_Lolium_perenne_GENOTYPES.vcf > /${VOLUME}/SIMULATED/DNA/Simulated_Lolium_perenne_GENOTYPES.vcf.gz
tabix -p vcf /${VOLUME}/SIMULATED/DNA/Simulated_Lolium_perenne_GENOTYPES.vcf.gz
rm /${VOLUME}/SIMULATED/DNA/Simulated_Lolium_perenne_GENOTYPES.vcf

##################################
## 	  SIMULATE TRANSCRIPTOME	##
##  __simulateTranscripts__.sh 	##
##################################

###############################
## 	 SIMULATE PHENOTYPES 	 ##
## __simulatePhenotypes__.sh ##
############################### includes pooling
~/SCRIPTS/__simulatePhenotypes__.sh ${VOLUME} Simulated_Lolium_perenne_GENOTYPES.data $nqtl $h2 $npool $MODEL
#INPUTS:
#	(1) working directory where the SIMULATED directoty containing DNA and RNA folders are located
#	(2) numeric genotype file ---> Simulated_Lolium_perenne_GENOTYPES.data
#	(3) number of QTLs to simulate
#	(4) heritability in percent whole number
#	(5) number of pools to use to divide the individuals into
#	(6) phenotype model to use
#	(7) transcriptome file 1: base levels -- use if model > 1
#	(8) transcriptome file 1: genotype-specific levels --- use if model > 1
#OUTPUTS:
#	(1) Simulated_Lolium_perenne_PHENOTYPES.data ---> headerless phenotypic values
#	(2) Simulated_Lolium_perenne_PHENOTYPES_POOLED.index ---> index where each genotype belongs to which pool
#	(3) Simulated_Lolium_perenne_PHENOTYPES.py ---> input phenotype stats for GWAlpha
#	(4) Simulated_Lolium_perenne_QTL.data ---> the locations (scaffold and positions) and effects of simulated QTL
#pooling individuals by phenotypes: [#worry not about the geno names in the vcf files - phenotyping and pooling counted them consecutively and does not correstpond to the vcf geno names]
cd /${VOLUME}/SIMULATED/DNA
cut -d$'\t' -f1,2 Simulated_Lolium_perenne_PHENOTYPES_POOLED.index | sed 's/\.0//g' > INDEX.temp
cut -d$'\t' -f3- Simulated_Lolium_perenne_GENOTYPES.data > GENOTYPE.temp
for i in $(seq $npool)
do
awk -v VAR="$i" '{if ($1==VAR) print $2}' INDEX.temp | sed ':a;N;$!ba;s/\n/,/g' > POOL_US.temp
cut -d$'\t' -f$(cat POOL_US.temp) GENOTYPE.temp > POOLED_GENO.temp
~/SCRIPTS/__extractAlleleFreqPerPOOL__.py /${VOLUME}/SIMULATED/DNA POOLED_GENO.temp
#INPUTS:
#	(1) working drectory: folder where the numeric genotype file is located and where the output will written on
#	(2) numeric genotype data (rows:loci, columns:genotypes), header=GENOTYPES
#OUTPUT:
#	(a) FREQ.temp --> reference allele frequency per loci
mv FREQ.temp FREQ-POOL-${i}.temp
done
tail -n+2 Simulated_Lolium_perenne_GENOTYPES.data | cut -d$'\t' -f1,2 > POS_NAMES.temp
paste -d$'\t' POS_NAMES.temp FREQ-POOL-*.temp > Simulated_Lolium_perenne_FREQ.pools
rm *.temp

###########################
## 	 SIMULATE RADseq 	 ##
## __simulateRADseq__.sh ##
########################### NOTE: does not reflect SNPs on the RE cut site... maybe go to compiled C/C++ so make whole genome fasta simulations more efficicient 20170927
cd ~/SOFTWARES/ddRADseqTools/Package*/
cat > Renzyme.txt << EOF
${ENZYME1}
${ENZYME2}
EOF
~/SCRIPTS/__simulate_RADseq_single_genome_20170925__.sh $VOLUME $MINFRAG $MAXFRAG $INSLEN $MUTPROB $LOCNUM $READNUM $rep $KEEP_DEPTH
#INPUTS:
#	(1) directory where the SIMULATED folder containing the DNA and RNA folders are located
#	(2) minimum fragment size to select after RE digest but before sequencing
#	(3) maximum fragment size to select after RE digest but before sequencing
#	(4) length of fragments for adaptor ligation and sequencing
#	(5) sequencing mutation probability in decimal form
#	(6) number of loci to capture
#	(7) number of rsequencing reads
#	(8) prefix of output filename
#	(9) keep depth file? 1 for yes and 0 for no
#OUTPUT:
#	(a) ${FILE_PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt --> RADseq stats
mv /${VOLUME}/SIMULATED/DNA/${rep}-${ENZYME1}x${ENZYME2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt /${VOLUME}/SIMULATED/DNA/${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.stats
mv /${VOLUME}/SIMULATED/DNA/${rep}-${ENZYME1}x${ENZYME2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv /${VOLUME}/SIMULATED/DNA/DEPTH.temp
mv /${VOLUME}/SIMULATED/DNA/${rep}-${ENZYME1}x${ENZYME2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-REDIGEST.csv /${VOLUME}/SIMULATED/DNA/${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.redigest

#########################
## 	 GENERATE SYNC 	   ##
## __generateSYNC__.sh ##
#########################
cd /${VOLUME}/SIMULATED/DNA/
awk -v OFS='\t' '{if ($3!=0) print $1,$2,$3}' DEPTH.temp > READS.temp1
cut -d$'\t' -f1 READS.temp1 | uniq > scaffolds.temp1
for scaf in $(cat scaffolds.temp1)
do
#echo $scaf
grep $scaf READS.temp1 > RADseq_ALLPOS.temp
grep $scaf Simulated_Lolium_perenne_FREQ.pools > R_INPUT_GENpool.temp
grep $scaf Simulated_Lolium_perenne_LOCI.data > R_INPUT_LOCIdata.temp

cut -d$'\t' -f2 R_INPUT_GENpool.temp > GENpoolPOS.temp
touch R_INPUT_RADseq.temp
for pos in $(cat GENpoolPOS.temp)
do
grep $'\t'$pos$'\t' RADseq_ALLPOS.temp >> R_INPUT_RADseq.temp
done
if [ $(cat R_INPUT_RADseq.temp | wc -l) -gt 0 ]
then
~/SCRIPTS/__generateSYNC__.py /${VOLUME}/SIMULATED/DNA/ R_INPUT_RADseq.temp R_INPUT_GENpool.temp R_INPUT_LOCIdata.temp
#INPUTS:
#	(1) working drectory: folder where the numeric genotype file is located and where the output will written on
#	(2) RADseq scaffold name, positions and coverage ---> R_INPUT_RADseq.temp
#	(3) simulated variation scaffold name, positions and reference allele frequencies per pool ---> R_INPUT_GENpool.temp
#	(4) simulated variation scaffold name, posisiotns, reference allele state and alternate allele state ---> R_INPUT_LOCIdata.temp
#OUTPUT:
#	(a) SYNC.temp ---> sync file: scaffold name, position, reference allele state, counts of A:C:T:G:N:DEL per pool (a column per pool)
mv SYNC.temp SCAFFOLD-${scaf}-OUT.sync
fi
rm *.temp
done
cat SCAFFOLD* > Simulated_Lolium_perenne_RADseq.sync
rm SCAFFOLD*
rm *.temp1

##########################
## ASSOSIATION ANALYSIS ##
##		GWAlpha.py 		##
##########################
mv Simulated_Lolium_perenne_RADseq.sync ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.sync
mv Simulated_Lolium_perenne_PHENOTYPES.py ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_pheno.py
~/SOFTWARES/GWAlpha/GWAlpha.py ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}.sync ML
#INPUTS:
#	(1) sync file of genotypes with the same name as the phenotype file in phyton format with the suffix "_pheno.py"
#	(2) paramater estimation method to use LS for least squares or V(e) minimization and ML for maximum likelihood or maximizxation of P(parameters | data) ===> argmax(Prob.DATA as a function of parameters)
#OUTPUT:
#	(a) GWAlpha-{sync filenames}_out.csv ---> identitites of each loci and their corresponding alpha, MAF and COV values | higher alpha^2 the more significant the loci
~/SCRIPTS/GWAlphaPlot-jefdited.r GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_out.csv pval qqplot
#INPUTS:
#	(1) GWAlphay.py output --> GWAlpha-{sync filenames}_out.csv
#	(2) metric to use: alpha or pval
#	(3) plot qqlpot
#OUTPUTS:
#	(a) GWAlpha-{sync filenames}.png ---> manhattan plot
#	(b) GWAlpha-{sync filenames}_qqplot.png ---> QQ-plot optional
~/SCRIPTS/plotROC.py /${VOLUME}/SIMULATED/DNA/  GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_out.csv Simulated_Lolium_perenne_QTL.data 1000
#INPUTS:
#	(1) working directory where the GWAlpha.py output is found
#	(2) GWAlphay.py output --> GWAlpha-{sync filenames}_out.csv
#	(3) locations (scaffold and positions) and effects of simulated QTL ---> Simulated_Lolium_perenne_QTL.data
#OUTPUT:
#	(a) ROC.png ---> receiver operating characteristing with AUC metric
mv GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_out.csv ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_GWAlpha.csv
mv GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_manhattan.png ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_manhattan.png
mv GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_qqplot.png ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_qqplot.png
mv ROC.png ${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_ROC.png
rm Simulated* GWAlpha*
done
done
done
done

#ULTIMATE OUTPUTS:
#	(a) poooled phenotype file in python format
#	(b) pooled genotype file in sync format
#	(c) RE digest stats: fragment sizes
#	(d) RADseq stats: REP, RE1, RE2, MINFRAG, MAXFRAG, INSLEN, NUMREADS, TOTALBASES, NUMREADS, NUMREADS^2
#	(e) GWAlpha manhattan plot
#	(f) GWAlpha QQ plot
#	(g) GWAlpha ROC plot
