args = commandArgs(trailing=TRUE)

GEN = read.table(file=args[1], header=T)
POOL = args[2]

# #for testing:
# GEN = read.table(file="POOLED_GENO.temp", header=T)
# POOL=1

MEANS = rowMeans(GEN)/2
write.table(MEANS, file=paste("FREQ-POOL-", POOL, ".temp", sep=""), row.names=F, col.names=F)