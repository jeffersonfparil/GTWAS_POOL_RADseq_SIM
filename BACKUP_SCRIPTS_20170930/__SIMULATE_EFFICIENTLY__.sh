#!/bin/bash
##################################
### run BAREST essentials only 20170925
#		QUESTION TO ANSWER: Which RE pair and insert length combination minimizes genome coverage variance or maximizes coverage?

#SET WORKING VOLUME
VOLUME=mnt

#MACHINE STATS
mem=$(($(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1000000))-5))
thr=$(($(grep -c ^processor /proc/cpuinfo)-1))

#CHMOD ALL SCRIPTS
chmod +x ~/SCRIPTS/*.py; chmod +x ~/SCRIPTS/*.sh; chmod +x ~/SCRIPTS/*.R

#FIX REFERENCE GENOME FORMAT
~/SCRIPTS/fixREF.py /home/ubuntu/SEQUENCES/DNA/ lope_V1.0.fasta Lolium_perenne_genome_FIXED.fasta

#PREPARE REFERENCE GENOME
cd ~/SEQUENCES/DNA/
cp Lolium_perenne_genome_FIXED.fasta /${VOLUME}/SIMULATED/DNA/
cd /${VOLUME}/SIMULATED/DNA/
bwa index -p Lolium_perenne_genome -a bwtsw Lolium_perenne_genome_FIXED.fasta
samtools faidx Lolium_perenne_genome_FIXED.fasta; mv Lolium_perenne_genome_FIXED.fasta.fai Lolium_perenne_genome.fai

# FOR ALL POSSIBLE RESTRICTION ENZYME PAIR:
cd ~/SOFTWARES/ddRADseqTools/Package*/
cat > Renzyme.txt << EOF
AatII
Acc65I
AclI
AflII
AgeI
ApaI
ApaLI
AscI
AseI
AsiSI
AvrII
BamHI
BclI
BglII
BmtI
BsiWI
BspEI
BspHI
BsrGI
BssHII
BstBI
ClaI
EagI
EcoRI
FseI
HindIII
KpnI
MfeI
MluI
MseI
NarI
NcoI
NdeI
NgoMIV
NheI
NotI
NsiI
PacI
PciI
PspOMI
PstI
PvuI
SacI
SacII
SalI
SbfI
SpeI
SphI
XbaI
XhoI
XmaI
EOF

#MAIN OUTPUT FILE
echo -e "TEST\tMIN_FRAG\tMAX_FRAG\tINSERT_LEN\tENZYME_1\tENZYME_2\tTOTAL_BASES\tTOTAL_READS\tTOTAL_SQUARED_READS" > /${VOLUME}/SIMULATED/DNA/RADseq_SIMULATION.out

#COMMON PARAMETERS
cd ~/SCRIPTS
MINFRAG=100
MAXFRAG=800
MUTPROB=0.80
LOCNUM=200000
READNUM=10000000
#PREFIX=TEST_INIT
#PREFIX=${1}
NREP=10
KEEP_DEPTH=0

for PREFIX in $(seq $NREP)
do
	#ITERATE ACROSS DIFFERENT INSERT LENGTHS [100 to 800 by 50]
	parallel ./__simulate_RADseq_single_genome_20170925__.sh $VOLUME $MINFRAG $MAXFRAG {1} $MUTPROB $LOCNUM $READNUM $PREFIX $KEEP_DEPTH ::: $(seq 100 50 800)
		#INPUT:
			# VOLUME=$1
			# MINFRAG=$2
			# MAXFRAG=$3
			# INSLEN=$4******PARALLELIZING THROUGH INSERT LENGTHS!!!!
			# MUTPROB=$5
			# LOCNUM=$6
			# READNUM=$7
			# PREFIX=$8
			# KEEP_DEPTH=0
		#OUTPUT:
			# ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt --> RADseq stats
	cat /${VOLUME}/SIMULATED/DNA/${PREFIX}-* >> /${VOLUME}/SIMULATED/DNA/RADseq_SIMULATION.out
	rm /${VOLUME}/SIMULATED/DNA/${PREFIX}-*
done