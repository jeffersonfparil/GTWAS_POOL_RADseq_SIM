#!/bin/bash
#		(1) directory where Lolium_perenne_genome_FIXED.fasta is located
#		(2) number of variants per scaffold to simulate
#		(3) number of genotypes to simulate
#		(4) prefix of the genotype names
#(1) Simulate one vcf file with all the possible variants
./simulateVAR.py $1 \
Lolium_perenne_genome_FIXED.fasta \
Simulated_Lolium_perenne_VARIANT.fasta \
Simulated_Lolium_perenne_VARIANT.vcf \
$2
##############################################################################
#testing gnu parallel on variant simulation
# cd $1
# nSCAF=$(grep ">" Lolium_perenne_genome_FIXED.fasta | wc -l)
# grep -n ">" Lolium_perenne_genome_FIXED.fasta | cut -d: -f1 > cutOffs.temp
# cat Lolium_perenne_genome_FIXED.fasta | wc -l >> cutOffs.temp
# for i in $(seq 2 $(($nSCAF+1)))
# do
# START=$(head -n$(($i-1)) cutOffs.temp | tail -n1)
# END=$(($(head -n$i cutOffs.temp | tail -n1)-1))
# head -n$END Lolium_perenne_genome_FIXED.fasta | tail -n$(($(($END - $START))+1)) > chunk$(($i - 1)).fasta
# done

# cd ~/SCRIPTS
# parallel "./simulateVAR.py $1 chunk{1}.fasta VARIANT_chunk{1}.fasta VARIANT_chunk{1}.vcf $2" ::: $(seq $nSCAF)

# cd $1
# cat VARIANT_chunk*.fasta > Simulated_Lolium_perenne_VARIANT.fasta
# head -n 19 VARIANT_chunk1.vcf > Simulated_Lolium_perenne_VARIANT.vcf
# for i in $(seq $nSCAF)
# do
# tail -n+20 VARIANT_chunk${i}.vcf >> Simulated_Lolium_perenne_VARIANT.vcf
# echo $i
# echo $(tail -n+20 VARIANT_chunk${i}.vcf)
# done
##############################################################################

#(2) Simulate a number of genotypes and stack them all together in one vcf file
# nLOCI=$(grep "scaffold" ${1}/Simulated_Lolium_perenne_VARIANT.vcf | wc -l)
# ./simulateGEN.py $1 \
# ${nLOCI} \
# $2 \
# $3 \
# $4 \
# Simulated_Lolium_perenne_VARIANT.vcf \
# Simulated_Lolium_perenne_GENOTYPES.vcf
cd $1
nLOCI=$(grep "scaffold" ${1}/Simulated_Lolium_perenne_VARIANT.vcf | wc -l)

cd ~/SCRIPTS
./simulateGEN__V2a__.py $1 \
${nLOCI} \
$2 \
$3 \
$4 \
Simulated_Lolium_perenne_VARIANT.vcf

parallel ./simulateGEN__V2b__.py $1 phase.txt $2 Simulated_Lolium_perenne_GENOTYPES.freq ${4}{1} ::: $(seq $3)

#parallel ./simulateGEN__V2b__.py /mnt/SIMULATED/DNA phase.txt 20 Simulated_Lolium_perenne_GENOTYPES.freq GENO{1} ::: {1..5}
# work_DIR = "/mnt/SIMULATED/DNA"
# phaseFile = "phase.txt"
# varPerScaf = 20
# freqFile = "Simulated_Lolium_perenne_GENOTYPES.freq"
# outputFilenamePlusID = "simGen.txt"

#concatenate genotypes
cd $1
paste -d$'\t' GENO* >> Simulated_Lolium_perenne_GENOTYPES.vcf.temp
echo ' ' > newline
cat newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline Simulated_Lolium_perenne_GENOTYPES.vcf.temp > binGEN.temp
paste -d$'\t' Simulated_Lolium_perenne_VARIANT.vcf binGEN.temp > Simulated_Lolium_perenne_GENOTYPES.vcf
rm GENO* *.temp newline phase.txt

#extract basic vcf file statistics including LD in R^2

vcftools --vcf Simulated_Lolium_perenne_GENOTYPES.vcf
vcftools --vcf Simulated_Lolium_perenne_GENOTYPES.vcf --freq --out Simulated_Lolium_perenne_GENOTYPES.vcfTOOLS
if [ $nLOCI -gt 10000 ]
then
head -n10000 Simulated_Lolium_perenne_GENOTYPES.vcf > forLD_extract.vcf
else
head -n$((19 + $nLOCI)) Simulated_Lolium_perenne_GENOTYPES.vcf > forLD_extract.vcf
fi
vcftools --vcf forLD_extract.vcf --hap-r2 --out Simulated_Lolium_perenne_GENOTYPES_PHASED.vcfTOOLS
cut -d$'\t' -f5 Simulated_Lolium_perenne_GENOTYPES.vcfTOOLS.frq |cut -d: -f2 > allele.freq
#plotting LD and allele frequencies
~/SCRIPTS/plotLDandAlleleFreq.R $1 allele.freq Simulated_Lolium_perenne_GENOTYPES_PHASED.vcfTOOLS.hap.ld
rm allele.freq forLD_extract.vcf

############################
##testing alternative LD model with multivariate normal distribution
#head -n10000 Simulated_Lolium_perenne-alternative_LD_MODEL.vcf > forLD_extract.vcf
#vcftools --vcf forLD_extract.vcf --hap-r2 --out Simulated_Lolium_perenne-alternative_LD_MODEL_PHASED.vcfTOOLS
#cut -d$'\t' -f5 Simulated_Lolium_perenne_GENOTYPES.vcfTOOLS.frq |cut -d: -f2 > allele.freq
##plotting LD and allele frequencies
#~/SCRIPTS/plotLDandAlleleFreq.R $1 allele.freq Simulated_Lolium_perenne-alternative_LD_MODEL_PHASED.vcfTOOLS.hap.ld
#rm allele.freq forLD_extract.vcf
############################

#	extracting DATA
grep "scaffold" Simulated_Lolium_perenne_GENOTYPES.vcf | cut -d$'\t' -f1,2,4,5 > Simulated_Lolium_perenne_LOCI.data
grep "scaffold" Simulated_Lolium_perenne_GENOTYPES.vcf | cut -d$'\t' -f1,2 > geno.loci
grep "scaffold" Simulated_Lolium_perenne_GENOTYPES.vcf | cut -d$'\t' -f10- > geno.data
paste -d$'\t' geno.loci geno.data > geno.txt
sed 's/0|0/2/g' geno.txt | sed 's/1|1/0/g' | sed 's/1|0/1/g' | sed 's/0|1/1/g' >> geno.num
#	extracting headers or genotype names
grep "#CHROM" Simulated_Lolium_perenne_GENOTYPES.vcf | cut -d$'\t' -f8- > header.data
sed 's/INFO/LOCI/g' header.data | sed 's/FORMAT/POS/g'> header.txt
#	pineapple - apple uhhmmmm!
cat header.txt geno.num > Simulated_Lolium_perenne_GENOTYPES.data
#(3) Extract binary genotype data prior assigning phenotypic values
cut -d$'\t' -f3- Simulated_Lolium_perenne_GENOTYPES.data > Simulated_Lolium_perenne_GENOTYPES.numeric

#	clean up
rm geno*
rm header*
#(4) Generate whole genome sequences from the vcf of simulated genotypes prior to simulating RADseq
cd $1
bgzip -c Simulated_Lolium_perenne_GENOTYPES.vcf > Simulated_Lolium_perenne_GENOTYPES.vcf.gz
tabix -p vcf Simulated_Lolium_perenne_GENOTYPES.vcf.gz
#Attempting parallelization at this last step.. no dependencies no problems :-|
#generating diploid fasta sequences - 2 sequences per diploid individual
genPREFIX=$4
parallel bcftools consensus -s ${genPREFIX}{1} -H 1 -f Lolium_perenne_genome_FIXED.fasta -I Simulated_Lolium_perenne_GENOTYPES.vcf.gz "|" bgzip -c ">" Simulated_Lolium_perenne-${genPREFIX}{1}-hap1.fasta.gz ::: $(seq $3)
parallel bcftools consensus -s ${genPREFIX}{1} -H 2 -f Lolium_perenne_genome_FIXED.fasta -I Simulated_Lolium_perenne_GENOTYPES.vcf.gz "|" bgzip -c ">" Simulated_Lolium_perenne-${genPREFIX}{1}-hap2.fasta.gz ::: $(seq $3)
