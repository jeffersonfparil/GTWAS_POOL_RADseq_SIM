#!/bin/bash
VOLUME=mnt

sudo add-apt-repository ppa:marutter/rrutter
sudo apt update

cd ~
mkdir SOFTWARES
mkdir BLAST
mkdir SEQUENCES
mkdir SEQUENCES/DNA
mkdir SEQUENCES/RNA
mkdir SEQUENCES/PRO
mkdir SCRIPTS

cd /${VOLUME}
mkdir SIMULATED
mkdir SIMULATED/DNA
mkdir SIMULATED/RNA
mkdir SIMULATED/PRO
mkdir SIMULATED/PHE

#(02)########## set up softwares
homeDIR=/home/ubuntu
cd ${homeDIR}/SOFTWARES
sudo apt install -y make libncurses5-dev libncursesw5-dev python3-dev python-dev libbz2-dev liblzma-dev python3-numpy python-numpy unzip git make default-jre build-essential zlib1g-dev bwa vcftools r-base htop python-pip python3-pip python-tk python3-tk parallel libboost-all-dev bedtools
pip install numpy scipy matplotlib Biopython statsmodels pandas sklearn pymp-pypi pysam; pip install pysamstats opencv-python scikit-image
#pip3 install matplotlib
wget http://zzz.bwh.harvard.edu/plink/dist/plink-1.07-x86_64.zip
wget https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/current/sratoolkit.current-ubuntu64.tar.gz
wget -O ddRADseqTools.zip https://github.com/GGFHF/ddRADseqTools/archive/master.zip
git clone git://github.com/samtools/samtools.git
git clone git://github.com/samtools/htslib.git
git clone git://github.com/samtools/bcftools.git
wget https://github.com/broadinstitute/picard/releases/download/2.10.0/picard.jar
git clone https://github.com/zstephens/neat-genreads.git
wget -O popoolation2.zip https://sourceforge.net/projects/popoolation2/files/latest/download
scp -i ${homeDIR}/.ssh/KEY ubuntu@130.220.208.152://home/ubuntu/SOFTWARES/GenomeAnalysisTK-3.7-0.tar.bz2 . #### MODIFY ME!!!
git clone https://github.com/aflevel/GWAlpha.git
wget -O cufflinks.tar.gz http://cole-trapnell-lab.github.io/cufflinks/assets/downloads/cufflinks-2.2.1.Linux_x86_64.tar.gz
git clone https://github.com/alexdobin/STAR.git
git clone https://github.com/danforthcenter/plantcv.git

unzip plink*.zip
tar -vxzf sratoolkit.current-ubuntu64.tar.gz
unzip ddRADseqTools.zip
unzip popoolation2.zip
tar xvjf GenomeAnalysisTK*.tar.bz2
tar xvzf cufflinks*.tar.gz
rm -R *.tar*
rm -R *.zip*

chmod +x GWAlpha/*.py 
chmod +x GWAlpha/*.r
mv sratoolkit* sratoolkit
mv ddRADseqTools-master ddRADseqTools
chmod +x ddRADseqTools/Package*/*.py
cd samtools
make
sudo make install
cd ../htslib
make
sudo make install
cd ../bcftools
make
sudo make install

export PATH=$PATH:${homeDIR}/SOFTWARES/sratoolkit/bin

#(03)##################### pull the reference genome
cd ${homeDIR}/SEQUENCES/DNA/
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/735/685/GCA_001735685.1_ASM173568v1/GCA_001735685.1_ASM173568v1_genomic.fna.gz
wget http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_V1.0.fasta
gzip -d  GCA_001735685.1_ASM173568v1_genomic.fna.gz
rm -R *.gz

cd ${homeDIR}/SEQUENCES/RNA/
wget http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_transcripts.V1.0.fasta	#predicted transcripts from the above genome: tissue and stress specific transcriptome can be found in another directory
wget http://185.45.23.197:5080/ryegrassdata/GENE_ANNOTATIONS/Lp_Annotation_V1.1.mp.gff3		#gene annotations

cd ${homeDIR}/SEQUENCES/PRO/
wget http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_proteins.V1.0.fasta
