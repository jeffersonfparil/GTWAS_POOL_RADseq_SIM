#!/usr/bin/env python
import os, subprocess, sys, math
import numpy as np

fileName = sys.argv[1]

skipMe = int(subprocess.check_output("echo $(grep -n '#CHROM' "+ fileName +" | cut -d: -f1)", shell=True).replace("\n", ""))
numCols = int(subprocess.check_output("echo $(($(tail -n1 " + fileName + " | grep -o '\t' | wc -l) + 2))", shell=True).replace("\n", ""))
refContig = np.genfromtxt(fileName, delimiter="\t", skip_header=skipMe, usecols=0, dtype='string')
position = np.genfromtxt(fileName, delimiter="\t", skip_header=skipMe, usecols=1, dtype='int')
refAllele = np.genfromtxt(fileName, delimiter="\t", skip_header=skipMe, usecols=3, dtype='string')
altAllele = np.genfromtxt(fileName, delimiter="\t", skip_header=skipMe, usecols=4, dtype='string')
binData = np.genfromtxt(fileName, delimiter="\t", skip_header=skipMe, usecols=range(9,numCols-1), dtype='string')

nLoci = binData.shape[0]
nInd = binData.shape[1]

DATA = []
for i in range(nLoci):
	COUNT_homREF = binData[i,].tolist().count("0|0")
	COUNT_homALT = binData[i,].tolist().count("1|1")
	COUNT_het = binData[i,].tolist().count("1|0") + binData[i,].tolist().count("0|1")
	ALLELE_REF = refAllele[i]
	ALLELE_ALT = altAllele[i]
	if ALLELE_ALT != "<DEL>":
		ALLELE_ALT = ALLELE_ALT[0]
	if ALLELE_REF == ALLELE_ALT:
		ALLELE_REF = "<DEL>" # in case of insertion e.g. REF(C) - ALT(CACG)
	COUNT_REF = (2*COUNT_homREF) + (COUNT_het)
	COUNT_ALT = (2*COUNT_homALT) + (COUNT_het)
	A=T=C=G=N=INDEL=0
	if ALLELE_REF == "A":
		A = COUNT_REF
	elif ALLELE_REF == "T":
		T = COUNT_REF
	elif ALLELE_REF == "C":
		C = COUNT_REF
	elif ALLELE_REF == "G":
		G = COUNT_REF
	elif ALLELE_REF == "N":
		N = COUNT_REF
	else:
		INDEL = COUNT_REF
	if ALLELE_ALT == "A":
		A = COUNT_ALT
	elif ALLELE_ALT == "T":
		T = COUNT_ALT
	elif ALLELE_ALT == "C":
		C = COUNT_ALT
	elif ALLELE_ALT == "G":
		G = COUNT_ALT
	elif ALLELE_ALT == "N":
		N = COUNT_ALT
	else:
		INDEL = COUNT_ALT
	DATA.append(str(A)+":"+str(T)+":"+str(C)+":"+str(G)+":"+str(N)+":"+str(INDEL))

OUT = np.column_stack((refContig, position, refAllele, DATA))
np.savetxt(fileName.replace("vcf", "sync"), OUT, fmt='%s' ,delimiter="\t")
