#!/usr/bin/env python
import os, sys, math, random
import numpy as np
from Bio import SeqIO
from Bio import Seq

work_DIR = sys.argv[1]
input_SEQ = sys.argv[2]
output_SEQ = sys.argv[3]
output_VCF = sys.argv[4]
varPerScaf = int(sys.argv[5])	#number of variants per scaffold
os.chdir(work_DIR)

#(1->2) SIMULATE ALLELIC VARIANTS FROM A REFERENCE GENOME AND 
#	 SPIT OUT THE VCF AND FASTA FILES WITH THE SIMULATED MUTATIONS

fasta_sequences = SeqIO.parse(input_SEQ,'fasta')
fasta_index = SeqIO.index(input_SEQ,'fasta')
NEW_FASTA = []
VCF = np.array([np.repeat(0, 9)])
for fasta in fasta_sequences:
	name, sequence = fasta.id, str(fasta.seq)
	LEN = len(sequence)
	POS = np.random.choice(range(LEN), replace=False, size=varPerScaf)
	for i in range(len(POS)):
		availPOS = set(range(LEN)) - set(POS)
		while sequence[POS[i]] != "A" and sequence[POS[i]] != "T" and sequence[POS[i]] != "C" and sequence[POS[i]] != "G":
			POS[i] = np.random.choice(list(availPOS), replace=False, size=1)
	POS.sort()
	MUT = np.random.choice(["Substitution", "Duplication", "Deletion"], size=len(POS), p=[0.9, 0.01, 0.09])
	#CH0 = np.random.chisquare(0.3, varPerScaf)
	CH0 = np.random.choice([1], varPerScaf)	#bcftools does not seem to like more than 1 base in the reference
	CH1 = np.random.chisquare(0.3, varPerScaf)
	CHR = []
	for cha in CH0:
		CHR.append(int(math.ceil(cha)))
	CHA = []
	for cha in CH1:
		CHA.append(int(math.ceil(cha)))
	REF=[]
	ALT=[]
	for i in range(len(POS)):
		if MUT[i] == "Substitution":
			SUB=[]
			for s in range(int(CHA[i])):
				SUB.append(random.choice(["A", "T", "C", "G"]))
			while "".join(SUB) == sequence[POS[i]:POS[i]+CHR[i]]:
				SUB=[]
				for s in range(int(CHA[i])):
					SUB.append(random.choice(["A", "T", "C", "G"]))
			sequence2 = sequence[:POS[i]] + "".join(SUB) + sequence[POS[i]+CHR[i]:]
			REF.append(sequence[POS[i]:POS[i]+CHR[i]])
			ALT.append("".join(SUB))
		else:
			if MUT[i] == "Duplication":
				sequence2 = sequence[:POS[i]+CHR[i]] + sequence[POS[i]:POS[i]+CHR[i]] + sequence[POS[i]+CHR[i]+1:]
				REF.append(sequence[POS[i]:POS[i]+CHR[i]])
				ALT.append(sequence[POS[i]:POS[i]+CHR[i]] + sequence[POS[i]:POS[i]+CHR[i]])
			else:
				sequence2 = sequence[:POS[i]] + sequence[POS[i]+1:]
				#REF.append(sequence[POS[i]-1:POS[i]+CHR[i]])
				#ALT.append(sequence2[POS[i]-1:POS[i]])
				REF.append(sequence[POS[i]:POS[i]+CHR[i]])
				ALT.append('<DEL>')
	fasta.seq = Seq.Seq(sequence2)
	NEW_FASTA.append(fasta)
	CHROM = np.repeat(name, varPerScaf)
	POS = POS + 1
	ID = np.repeat(".", varPerScaf)
	QUAL = np.repeat(".", varPerScaf)
	FILTER = np.repeat("PASS", varPerScaf)
	INFO = np.repeat(".", varPerScaf)
	FORMAT = np.repeat("GT", varPerScaf)
	vcf = np.stack((CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO, FORMAT), axis=-1)
	VCF = np.concatenate((VCF, vcf), axis=0)

#FASTA OUTPUT:
SeqIO.write(NEW_FASTA, output_SEQ, "fasta")
#WRITE VCF FILE:
VCF = VCF[1:len(VCF)]
np.savetxt("outPy.txt", VCF, fmt='%s' ,delimiter="\t")
os.system("cat VCF.header outPy.txt >  " + output_VCF)
os.system("rm outPy.txt")
###:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::###