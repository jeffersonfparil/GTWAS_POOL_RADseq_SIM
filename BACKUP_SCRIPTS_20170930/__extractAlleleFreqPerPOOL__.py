#!/usr/bin/env python
import os, subprocess, sys, math
import numpy as np

# #for testing:
# DIR = "/mnt/SIMULATED/DNA"
# fileName = "POOLED_GENO.temp"

DIR = sys.argv[1]
fileName = sys.argv[2]

os.chdir(DIR)
GEN = np.genfromtxt(fileName, delimiter='\t', skip_header=1)
FREQ = np.mean(GEN, axis=1) / 2

np.savetxt("FREQ.temp", FREQ, fmt="%s", delimiter="\t")