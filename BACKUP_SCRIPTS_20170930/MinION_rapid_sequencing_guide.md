---
title: MinION rapid sequencing guide
subtitle: adapted from https://community.nanoporetech.com/guides/minion/rapid/
author: AFL Laboratory
date: 2018 June 28
documentclass: scrartcl
papersize: a4
geometry: "left=1cm, right=1cm, top=2cm, bottom=2cm"
fontsize: 9pt
tags: [minion, MinKNOW, nanopore, nanoporetech, Oxford Nanopore]
header-includes:
    - \usepackage{multicol}
    - \newcommand{\hideFromPandoc}[1]{#1}
    - \hideFromPandoc{
        \let\Begin\begin
        \let\End\end
      }
---

\Begin{multicols}{2}

# Software installation

## MinKNOW GUI and daemon
~~~~~~~~~~
sudo apt update
sudo apt install wget
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt update
sudo apt install minknow-nc
~~~~~~~~~~

## Albacore (basecalling software)
~~~~~~~~~~
wget https://mirror.oxfordnanoportal.com/software/analysis/python3-ont-albacore_2.3.1-1~xenial_amd64.deb
sudo dpkg -i python3-ont-albacore*.deb
~~~~~~~~~~

Several error messages will be printed out which will be fixed by installing missing dependencies via:

~~~~~~~~~~
sudo apt-get -f install
~~~~~~~~~~

# Running

## MinKNOW GUI
~~~~~~~~~~
cd /opt/ONT/MinKNOW
sudo bin/mk_manager_svc
~~~~~~~~~~

then in a separate terminal:

~~~~~~~~~~
MinKNOW
~~~~~~~~~~

or

~~~~~~~~~~
sudo systemctl daemon-reload
sudo systemctl enable minknow
sudo systemctl start minknow
MinKNOW
~~~~~~~~~~

# Updating
~~~~~~~~~~
sudo apt update
sudo apt install minknow-nc
~~~~~~~~~~

# Removal
~~~~~~~~~~
sudo apt purge minknow-nc
sudo apt autoremove
sudo apt clean
~~~~~~~~~~

# Hardware check

- plug in the device into a USB 3.0 port (blue)
- start MinKNOW 
- check hardware by ticking the "Available" box and click on "Check hardware" button

# Flow cell check

- insert flow cell into the clip ensuring good contact between the flow cell and the MinION and close the lid
- select flow cell type (i.e. FLO-MIN106 or FLO-MIN107)
-  tick the available box and click on "Check flow cells" button

# Library and sequencing run preparations

- using the *Rapid Sequencing Kit* (10 minutes)
- Materials
    + 0.2ml thin walled PCR tubes
    + 1.5ml Eppendorf DNA LoBind tubes
    + *Flow cell priming kit* (EXP-FLP001)
    + ice bucket with ice
    + microfuge
    + nuclease-free water
    + P2 pipette and tips
    + P20 pipette and tips
    + P100 pipette and tips
    + P1000 pipette and tips
    + *Rapid sequencing kit* (SQK-RAD004)
    + Thermal cycler at 30&deg;C and 80&deg;C
    + Timer
    + Vortex mixer
- Defrost at room temperature
    + Lambda DNA (sample DNA)
    + sequencing buffer (SQB)
    + loading beads (LB)
    + 1 tube flush buffer (FLB)
    + flush tether (FLT)
    + nuclease-free water (NFW)
    + (Notice that fragmentation mix, FRA and rapid adapter, RAP are not frozen)
- Mix contents by flicking, spin down and keep on ice

## Prepare library (Tube 1)

- Program thermal cycler to run at 30&deg;C for 1 minute, then 80&deg;C for 1 minute
- Take one 0.2ml PCR tube and one 1.5ml Eppendorf DNA LoBind tubes. Labels as:
    + 0.2ml PCR tube as Tube 1
    + 1.5ml Eppendorf DNA LoBind tube as Tube 2
- Transfer 7.5$\mu$l of Lambda DNA to Tube 1
- Add 2.5$\mu$l of FRA into Tube 1
- Mix contents by flicking and spin down
- Incubate Tube 1 at 30&deg;C for 1 minute then at 80&deg;C for 1 minute
- Spin down Tube 1 briefly and put on ice for about 30 seconds to cool down
- Add 1$\mu$l of RAP to Tube 1
- Mix Tube 1 by flicking and spin down
- Incubate for 5 minutes at room temperature
- Put on ice until use

## Prepare priming mix (FLB + 30$\mu$l FLT)

- Mix FLB and FLT by flicking and spin down
- Transfer 30$\mu$l of FLT into the 1 tube of FLB, mix by flicking and spin down
- Put on ice until use

## Prepare pre-sequencing mix (Tube 2)

- Transfer 34$\mu$l of SQB to Tube 2
- Add 4.5$\mu$l of NFW into Tube 2
- Resuspend LB by gently pipetting up and down using P100
- Add 25.5$\mu$l of LB into Tube 2
- Add 11$\mu$l of the prepared library (Tube 1) into Tube 2
- Mix Tube 2 by flicking and spin down
- Put on ice until use

# Prime and load the sample

- aims: prime your flow cell with buffer, load your DNA library into the flow cell, and start a sequencing experiment in MinKNOW
- Warnings:
    + **it is essential that the sensor array remains submerged in buffer at all times**
    + **if an air bubble passes over any channels, those pores will be permanently damaged**
    + **avoid introducing air in the pipette tips**
    + **do not fully expel the liquid from the tip - leave a small volume in the tip**
    + **remove any air bubbles in the flow cell ports**
    + **priming and loading sample into flow cells is best done with minimal movement**
    + **do the following steps on the MinION device on a flat surface**
- Open the priming port by sliding the cover clockwise
- Remove inlet channel air plug
    + set P1000 pipette to 200$\mu$l
    + insert the tip into the priming port
    + keeping the tip inserted in the port, twist the when of the pipette until you reach 220-230$\mu$l, or until you see a small amount of the yellow buffer in the tip
    + remove pipette from the port and discard the tip
- Flow cell priming
    + load 800$\mu$l of the priming mix (FLB + 30$\mu$l FLT) into the priming port slowly
    + do not load all the mix keep a small amount at the tip
    + close the priming port
    + wait for 5 minutes
- Open the priming port by sliding the cover clockwise
- Open the sample port by gently lifting the sample port cover
- Load 200$\mu$l of the priming mix (FLB + 30$\mu$l FLT) into the priming port using P1000 (do not empty the tip)
- Resuspend pre-sequencing mix (Tube 2) by gently pipetting up and down with P200
- Load 75$\mu$l of the the pre-sequencing mix (Tube 2) into the sample port using P200 **drop by drop**
- Close the sample port with the cover (ensure the bung enters the port)\
- Close the priming port (turn counter-clockwise)
- Close the MinION lid carefully (magnetic at the edge)

# Sequencing run

- Tick the "Available" box and choose the proper flow cell type (i.e. FLO-MIN106 or FLO-MIN107)
- Click "New Experiment" and in the pop-up screen:
    + enter experiment name
    + select the proper kit (SQK-RAD004 for Lambda DNA control test)
    + turn basecalling on or off depending on your machine (at least 16GB RAM and 1TB SSD is needed if you want this turned on)
    + set run options or keep default at 48 hours and -180mV
    + specify output files (just .fast5 files for separate basecalling; the number of files written per folder can also be specified)
    + Click "Start run"
- When sequencing is initiated:
    + the software will move to the channel screen
    + flow cell will cycle through all 2048 channels to select the best set of 512 to start the run
    + once the best combination of channels is selected, sequencing will begin on those channels automatically
    + within the first half an hour you should have most active pores sequencing and already have obtained several thousand reads
- After 6 hours (or however long you set):
    + you can stop the run from the home screen
    + output will be in /var/lib/MinKNOW/data/reads 

# Basecalling

- Albacore:

~~~~~~~~~~~
read_fast5_basecaller.py \
    --flowcell FLO-MIN107 \
    --kit SQK-LSK108 \
    --barcoding \
    --output_format fast5,fastq \
    --input directory_of_fast5_files \
    --save_path directory_for_output \
    --worker_threads 4 
~~~~~~~~~~~~

\End{multicols}
