#!/bin/bash
#generate the numeric transcript data
cd ~/SCRIPTS
./simulateTRANS1.py $1 Lolium_perenne_transcriptome.fasta $2 $3
#generate the fasta trascript files
parallel ./simulateTRANS2.py $1 Lolium_perenne_transcriptome.fasta $2 $3 Simulated_Lolium_perenne_TRANSCRIPT_BASE.data Simulated_Lolium_perenne_TRANSCRIPT_GENO.data {1} ::: $(seq $2)

#working progress: how to extract transcript abundance with cufflinks
# cd $1
# bwa index -p Lolium_perenne_transcriptome -a bwtsw Lolium_perenne_transcriptome.fasta
# genPREFIX=$3
# parallel bwa mem -M Lolium_perenne_transcriptome Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.fasta "|" samtools view "|" cut -f3 "|" uniq -c ">" Simulated_Lolium_perenne_TRANSCRIPT-COUNTS_${genPREFIX}{1}.txt ::: $(seq $2)
# cut -c1-7 Simulated_Lolium_perenne_TRANSCRIPT-COUNTS_GENO1.txt | sed 's/ //g' > Simulated_Lolium_perenne_TRANSCRIPT-COUNTS_ALL.txt
# for i in $(seq 2 $2)
# do
# paste -d$'\t' Simulated_Lolium_perenne_TRANSCRIPT-COUNTS_ALL.txt <(cut -c1-7 Simulated_Lolium_perenne_TRANSCRIPT-COUNTS_GENO${i}.txt | sed 's/ //g') > temp.txt
# mv temp.txt Simulated_Lolium_perenne_TRANSCRIPT-COUNTS_ALL.txt
# done

# rm Simulated_Lolium_perenne_TRANSCRIPT-COUNTS_GENO*

# parallel bwa mem -M Lolium_perenne_transcriptome Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.fastq ">" Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.sam ::: $(seq $2)

# rm Simulated_Lolium_perenne_TRANSCRIPT_GENO*.fastq
# #then use cufflinks!!!
# #maybe pipe them all together:
# samtools view -Sb Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.sam > Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.bam
# samtools sort Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.bam > Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.sorted
# mv Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.sorted Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.bam
# ~/SOFTWARES/cufflinks*/cufflinks -b Lolium_perenne_transcriptome.fasta Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.bam
# #it works! Now to pipe and parallelize it!

#parallel bwa mem -M Lolium_perenne_transcriptome Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.fastq "|"  samtools view -Sb  "|" samtools sort "|" /home/ubuntu/SOFTWARES/cufflinks*/cufflinks -b Lolium_perenne_transcriptome.fasta ::: $(seq $2)
#parallel bwa mem -M Lolium_perenne_transcriptome Simulated_Lolium_perenne_TRANSCRIPT_${genPREFIX}{1}.fastq "|"  samtools view -Sb  "|" samtools sort ">" test.sorted.bam ::: $(seq 2)

#USING STAR FOR MAPPING AND CUFFLINKS FOR TRANSCRIPT ABUNDANCE DATA EXTRACTION FROM FASTQ FILES (single-end - i should use paired-end right?! ;-P)
#(1) generate genome reference index?
cd $1
mkdir STAR_GENOME/
~/SOFTWARES/STAR/bin/Linux_x86_64_static/STAR \
--runThreadN 5 \
--runMode genomeGenerate \
--genomeDir STAR_GENOME/ \
--genomeFastaFiles Lolium_perenne_transcriptome.fasta
#--sjdbGTFfile ${1}/Lp_Annotation_V1.1.mp.gff3
#--sjdbOverhang ReadLength-1
#(2) map fastq reads
~/SOFTWARES/STAR/bin/Linux_x86_64_static/STAR \
--runThreadN 5 \
--genomeDir STAR_GENOME/ \
--readFilesIn Simulated_Lolium_perenne_TRANSCRIPT_RHONA2.fastq \
--outFileNamePrefix Simulated_Lolium_perenne_TRANSCRIPT_RHONA2_ \
--outSAMtype BAM SortedByCoordinate
#(3) transcript abundance data extraction
#~/SOFTWARES/cufflinks*/cufflinks -b Lolium_perenne_transcriptome.fasta -g Lolium_perenne_annotations.gff3 Simulated_Lolium_perenne_TRANSCRIPT_RHONA2_Aligned.sortedByCoord.out.bam > Simulated_Lolium_perenne_TRANSCRIPT_RHONA2_
~/SOFTWARES/cufflinks*/cufflinks -b Lolium_perenne_transcriptome.fasta Simulated_Lolium_perenne_TRANSCRIPT_RHONA2_Aligned.sortedByCoord.out.bam && mv genes.fpkm_tracking Simulated_Lolium_perenne_TRANSCRIPT_RHONA2_FPKM.out

#for testing:
# workDIR =  "/mnt/SIMULATED/RNA"genes.fpkm_tracking
# transcript = "Lolium_perenne_transcriptome.fasta"
# nIND = 500
# name = "GENO"
# indID = 1