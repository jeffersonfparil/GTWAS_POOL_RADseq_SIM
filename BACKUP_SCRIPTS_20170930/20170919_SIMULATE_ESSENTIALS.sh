#!/bin/bash
##################################
### run BARE essentials only 20170919
### OBJECTIVES of [1] GENOMICS:
###		(1) Which are the best RE pair and fragment length to use in RADseq? --> maximize number of scaffolds and SNPs captured!!!
###		(2) How many reads is best to represent the transcriptome? --> maximize number of aligned transcriptome with the 2017-Shinozuka L. perenne transcriptome
DIR=mnt
#chmod +x ~/SCRIPTS/*.py; chmod +x ~/SCRIPTS/*.sh; chmod +x ~/SCRIPTS/*.R
#~/SCRIPTS/fixREF.py /home/ubuntu/SEQUENCES/DNA/ lope_V1.0.fasta Lolium_perenne_genome_FIXED.fasta
#PREPARE REFERENCE GENOME
#cd ~/SEQUENCES/DNA/
#cp Lolium_perenne_genome_FIXED.fasta /${DIR}/SIMULATED/DNA/
#cd /${DIR}/SIMULATED/DNA/
#bwa index -p Lolium_perenne_genome -a bwtsw Lolium_perenne_genome_FIXED.fasta
#samtools faidx Lolium_perenne_genome_FIXED.fasta; mv Lolium_perenne_genome_FIXED.fasta.fai Lolium_perenne_genome.fai

# FOR ALL POSSIBLE REnzyme PAIR:
cd ~/SOFTWARES/ddRADseqTools/Package*/
cat > Renzyme.txt << EOF
AatII
Acc65I
AclI
AflII
AgeI
ApaI
ApaLI
AscI
AseI
AsiSI
AvrII
BamHI
BclI
BglII
BmtI
BsiWI
BspEI
BspHI
BsrGI
BssHII
BstBI
ClaI
EagI
EcoRI
FseI
HindIII
KpnI
MfeI
MluI
MseI
NarI
NcoI
NdeI
NgoMIV
NheI
NotI
NsiI
PacI
PciI
PspOMI
PstI
PvuI
SacI
SacII
SalI
SbfI
SpeI
SphI
XbaI
XhoI
XmaI
EOF

#MULTIPLEXING MASTERLIST
cat > poolsIND.txt << EOF
#RECORD FORMAT: individual_id;replicated_individual_id or NONE;population_id;index1_seq(5'->3');[index2_seq(5'->3')]
POOL1;NONE;pop01;GGTCTT;ATCACG
POOL2;NONE;pop01;CTGGTT;CGATGT
POOL3;NONE;pop01;AAGATA;TTAGGC
POOL4;NONE;pop01;ACTTCC;TGACCA
POOL5;NONE;pop01;TTACGG;ACAGTG
EOF

#MAIN OUTPUT FILE
echo -e "REP\tMIN_FRAG\tMAX_FRAG\tINSERT_LEN\tENZYME_1\tENZYME_2\tN_SCAFF\tTOTAL_SNP\tMEAN_DEPTH\tVAR_DEPTH" > ~/RADseq_SIMULATION.out

#COMMON PARAMETERS
cd ~/SCRIPTS
NPOOL=5
NREP=4
FRAGINT=25
MUTPROB=0.80
LOCINUM=200000
READSNUM=12500000
#min=100
#inc=100
#insertLen=120

#ITERATION ACROSS DIFFENT FRAGMENT AND INSERT LENGTHS
for min in $(seq 100 50 700)
do
	for inc in $(seq 50 10 100)
	do
		for insertLen in $(seq $min 10 $(($min + $inc)))
		do
			parallel ./20170919_simulateRADseq1genome__.sh ${min} ${inc} ${insertLen} {1} ${DIR} ${FRAGINT} ${MUTPROB} ${LOCINUM} ${READSNUM} ${NPOOL} ::: $(seq $NREP)
			cat ~/sim_* >> ~/RADseq_SIMULATION.out
			rm ~/sim_*
		done
	done
done
