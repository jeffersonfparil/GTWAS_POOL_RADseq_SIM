#!/usr/bin/env python
import os, sys
from Bio import SeqIO
DIR = sys.argv[1]
INPUT_fasta = sys.argv[2]
OUTPUT_fasta = sys.argv[3]

os.chdir(DIR)
SeqIO.convert(INPUT_fasta, "fasta", OUTPUT_fasta, "fasta")
