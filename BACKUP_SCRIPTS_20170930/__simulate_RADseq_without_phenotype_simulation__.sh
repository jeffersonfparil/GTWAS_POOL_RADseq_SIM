#!/bin/bash
VOLUME=$8
mkdir /${VOLUME}/SIMULATED/DNA_r${4}
cp ~/SEQUENCES/DNA/Lolium_perenne_genome_FIXED.fasta /${VOLUME}/SIMULATED/DNA_r${4}/

#PARAMETER INPUTS:
NIND=$5
VPSC=$6
NPOO=$7

##########################
#						 #
# simulate genotype data #
#						 #
##########################
cd ~/SCRIPTS
cp VCF.header /${VOLUME}/SIMULATED/DNA_r${4}
#./simulateGenotypes.sh /${VOLUME}/SIMULATED/DNA_r${4} $VPSC $NIND GENO
#		(1) directory where Lolium_perenne_genome_FIXED.fasta is located
#		(2) number of variants per scaffold to simulate
#		(3) number of genotypes to simulate
#		(4) prefix of the genotype names
#(1) Simulate one vcf file with all the possible variants
./simulateVAR.py /${VOLUME}/SIMULATED/DNA_r${4} \
Lolium_perenne_genome_FIXED.fasta \
Simulated_Lolium_perenne_VARIANT.fasta \
Simulated_Lolium_perenne_VARIANT.vcf \
$VPSC
#(2) Simulate a number of genotypes and stack them all together in one vcf file
cd /${VOLUME}/SIMULATED/DNA_r${4}
nLOCI=$(grep "scaffold" /${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_VARIANT.vcf | wc -l)
cd ~/SCRIPTS
./simulateGEN__V2a__.py /${VOLUME}/SIMULATED/DNA_r${4} \
${nLOCI} \
$VPSC \
$NIND \
GENO \
Simulated_Lolium_perenne_VARIANT.vcf
parallel ./simulateGEN__V2b__.py /${VOLUME}/SIMULATED/DNA_r${4} phase.txt $VPSC Simulated_Lolium_perenne_GENOTYPES.freq GENO{1} ::: $(seq $NIND)
cd /${VOLUME}/SIMULATED/DNA_r${4}
paste -d$'\t' GENO* >> Simulated_Lolium_perenne_GENOTYPES.vcf.temp
echo ' ' > newline
cat newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline Simulated_Lolium_perenne_GENOTYPES.vcf.temp > binGEN.temp
paste -d$'\t' Simulated_Lolium_perenne_VARIANT.vcf binGEN.temp > Simulated_Lolium_perenne_GENOTYPES.vcf
rm GENO* *.temp newline phase.txt
#(3) Generate whole genome sequences from the vcf of simulated genotypes prior to simulating RADseq ... overlapping with pooling for efficiency
bgzip -c Simulated_Lolium_perenne_GENOTYPES.vcf > Simulated_Lolium_perenne_GENOTYPES.vcf.gz
tabix -p vcf Simulated_Lolium_perenne_GENOTYPES.vcf.gz

###########################################################
#														  #
# Arbitrarily pool individuals (100 per pool for 5 pools) #
#														  #
############################################################## 2017 09 14 may need to optime thr for parallelization within parallel processes
cd /${VOLUME}/SIMULATED/DNA_r${4}
mem=$(($(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1000000))-5))
thr=$(($(grep -c ^processor /proc/cpuinfo)-1))
for i in $(seq $NPOO)
do
start=$(($(($(($i - 1))*$(($NIND/$NPOO))))+1))
end=$(($i*$(($NIND/$NPOO))))
for f in $(seq $start $end)
do
bcftools consensus -s GENO${f} -H 1 -f Lolium_perenne_genome_FIXED.fasta -I Simulated_Lolium_perenne_GENOTYPES.vcf.gz | bgzip -c >> Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap1.fasta.gz
bcftools consensus -s GENO${f} -H 2 -f Lolium_perenne_genome_FIXED.fasta -I Simulated_Lolium_perenne_GENOTYPES.vcf.gz | bgzip -c >> Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap2.fasta.gz
done
#parallel output into a common file creates conflicts!!!
#bcftools consensus -s GENO${start} -H 1 -f Lolium_perenne_genome_FIXED.fasta -I Simulated_Lolium_perenne_GENOTYPES.vcf.gz | bgzip -c > Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap1.fasta.gz
#bcftools consensus -s GENO${start} -H 2 -f Lolium_perenne_genome_FIXED.fasta -I Simulated_Lolium_perenne_GENOTYPES.vcf.gz | bgzip -c > Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap2.fasta.gz
#parallel bcftools consensus -s GENO{1} -H 1 -f Lolium_perenne_genome_FIXED.fasta -I Simulated_Lolium_perenne_GENOTYPES.vcf.gz "|" bgzip -c ">>" Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap1.fasta.gz ::: $(seq $(($start + 1)) 1 $end)
#parallel bcftools consensus -s GENO{1} -H 2 -f Lolium_perenne_genome_FIXED.fasta -I Simulated_Lolium_perenne_GENOTYPES.vcf.gz "|" bgzip -c ">>" Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap2.fasta.gz ::: $(seq $(($start + 1)) 1 $end)
cat Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap1.fasta.gz Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap2.fasta.gz > Simulated_Lolium_perenne_GENOTYPES-POOL${i}.fasta.gz
rm Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap1.fasta.gz Simulated_Lolium_perenne_GENOTYPES-POOL${i}-hap2.fasta.gz
done

# FOR ALL POSSIBLE REnzyme PAIR:
cd ~/SOFTWARES/ddRADseqTools/Package*/
cat > Renzyme.txt << EOF
AatII
Acc65I
AclI
AflII
AgeI
ApaI
ApaLI
AscI
AseI
AsiSI
AvrII
BamHI
BclI
BglII
BmtI
BsiWI
BspEI
BspHI
BsrGI
BssHII
BstBI
ClaI
EagI
EcoRI
FseI
HindIII
KpnI
MfeI
MluI
MseI
NarI
NcoI
NdeI
NgoMIV
NheI
NotI
NsiI
PacI
PciI
PspOMI
PstI
PvuI
SacI
SacII
SalI
SbfI
SpeI
SphI
XbaI
XhoI
XmaI
EOF

for k in $(seq $(cat Renzyme.txt | wc -l))
do
enzyme1=$(head -n$k Renzyme.txt | tail -n 1)

for j in $(seq $((k+1)) $(cat Renzyme.txt | wc -l))
do
enzyme2=$(head -n$j Renzyme.txt | tail -n 1)

# for i in $(seq $NPOO)
# do
# #simulate RE digetion
# ./rsitesearch.py \
# --genfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_GENOTYPES-POOL${i}.fasta.gz \
# --enzyme1=$enzyme1 --enzyme2=$enzyme2 \
# --minfragsize=$1 --maxfragsize=$(($1+$2)) \
# --fragstinterval=25 \
# --fragstfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-REDIGEST.txt \
# --fragsfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-FRAGMENTS.fasta &> /dev/null
# #simulate sequencing
# ./simddradseq.py \
# --fragsfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-FRAGMENTS.fasta \
# --individualsfile=poolsIND.txt \
# --technique=IND1_IND2_DBR \
# --enzyme1=$enzyme1 --enzyme2=$enzyme2 \
# --readtype=PE \
# --mutprob=0.05 \
# --insertlen=$3 \
# --locinum=200000 \
# --readsnum=12500000 \
# --format=FASTQ \
# --readsfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ILLREADS &> /dev/null
# done

#parallelization
#simulate RE digetion (rsitesearch.py) and simulate sequencing (simddradseq.py)
parallel ./rsitesearch.py \
--genfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_GENOTYPES-POOL{1}.fasta.gz \
--enzyme1=$enzyme1 --enzyme2=$enzyme2 \
--minfragsize=$1 --maxfragsize=$(($1+$2)) \
--fragstinterval=25 \
--fragstfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_POOL{1}-${enzyme1}x${enzyme2}-REDIGEST.txt \
--fragsfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_POOL{1}-${enzyme1}x${enzyme2}-FRAGMENTS.fasta "&>" /dev/null "&&" ./simddradseq.py \
--fragsfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_POOL{1}-${enzyme1}x${enzyme2}-FRAGMENTS.fasta \
--individualsfile=poolsIND.txt \
--technique=IND1_IND2_DBR \
--enzyme1=$enzyme1 --enzyme2=$enzyme2 \
--readtype=PE \
--mutprob=0.05 \
--insertlen=$3 \
--locinum=200000 \
--readsnum=12500000 \
--format=FASTQ \
--readsfile=/${VOLUME}/SIMULATED/DNA_r${4}/Simulated_Lolium_perenne_POOL{1}-${enzyme1}x${enzyme2}-ILLREADS "&>" /dev/null ::: $(seq $NPOO)


#variant calling with bcftools mpileup .. | bcftools call ..
cd /${VOLUME}/SIMULATED/DNA_r${4}
bwa index -p Lolium_perenne_genome -a bwtsw Lolium_perenne_genome_FIXED.fasta
samtools faidx Lolium_perenne_genome_FIXED.fasta; mv Lolium_perenne_genome_FIXED.fasta.fai Lolium_perenne_genome.fai
for i in $(seq $NPOO)
do
### PIPE IT! PIPE IT! PIPE IT! ;-P
#align them bunch of illumina reads into the reference genome built above
bwa mem -t $thr -M Lolium_perenne_genome \
Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ILLREADS-1.fastq \
Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ILLREADS-2.fastq \
> Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ALIGNED.sam
#compress the aligned sam file into a bam file
samtools view -@ $thr -Sb \
Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ALIGNED.sam \
> Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ALIGNED.bam
#sort these bam files for indexing
samtools sort -@ $thr -m ${mem}G Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ALIGNED.bam \
> Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ALIGNED.sorted
mv Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ALIGNED.sorted Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ALIGNED.bam
#build bam index, #remove duplicates and #rebuild cleaned bam index with picard
java -ea -Xmx${mem}g -jar ~/SOFTWARES/picard.jar BuildBamIndex \
I=Simulated_Lolium_perenne_POOL${i}-${enzyme1}x${enzyme2}-ALIGNED.bam &> /dev/null
done

#variant calling without duplicate removal
ls Simulated_Lolium_perenne_POOL*-${enzyme1}x${enzyme2}-ALIGNED.bam > bam.list
samtools mpileup -b bam.list \
> Simulated_Lolium_perenne_POOLED_GENOTYPES-${enzyme1}x${enzyme2}.mpileup
java -ea -Xmx${mem}g -jar ~/SOFTWARES/popoolation2*/mpileup2sync.jar \
--input Simulated_Lolium_perenne_POOLED_GENOTYPES-${enzyme1}x${enzyme2}.mpileup \
--output Simulated_Lolium_perenne_POOLED_GENOTYPES-${enzyme1}x${enzyme2}.sync.temp \
--fastq-type sanger \
--min-qual 30

# removing monomorphic loci (TEST 20170904 - awk:::alex's suggestion! Works perfectly! :-D)
awk -v OFS='\t' '{if ($4!=$5 || $5!=$6 || $6!=$7 || $7!=$8) print $1,$2,$3,$4,$5,$6,$7,$8}' Simulated_Lolium_perenne_POOLED_GENOTYPES-${enzyme1}x${enzyme2}.sync.temp > Simulated_Lolium_perenne_POOLED_GENOTYPES-${enzyme1}x${enzyme2}.sync

#INSERT ASSESSMENT STEPS HERE:
echo ${enzyme1}x${enzyme2} > Simulated_Lolium_perenne_RADseq_${enzyme1}x${enzyme2}_COVERAGE.csv
grep "scaffold" Simulated_Lolium_perenne_POOLED_GENOTYPES-${enzyme1}x${enzyme2}.sync | cut -f1 -d$'\t' > SCAF.list.temp
uniq SCAF.list.temp > SCAF.names.temp
uniq -c SCAF.list.temp | sed 's/ s/:s/g' | grep -n scaffold | cut -f2 -d: | sed 's/ //g' > SCAF.counts.temp
paste -d, SCAF.names.temp SCAF.counts.temp >> Simulated_Lolium_perenne_RADseq_${enzyme1}x${enzyme2}_COVERAGE.csv
#extract sequencing coverage with samtools
samtools depth -a Simulated_Lolium_perenne_POOL*-${enzyme1}x${enzyme2}-ALIGNED.bam > Simulated_Lolium_perenne_RADseq_DEPTH.csv
if [ $(cat Simulated_Lolium_perenne_RADseq_DEPTH.csv | wc -l) == 0 ]
then
aveDEPTH=0
varDEPTH=0
else
aveDEPTH=$(($(awk '{ total += $3 + S4 + $5 + $6 + $7 } END {print total}' Simulated_Lolium_perenne_RADseq_DEPTH.csv) / $(($(cat Simulated_Lolium_perenne_RADseq_DEPTH.csv | wc -l) * $NPOO))))
varDEPTH=$(($(($(awk '{ total += $3^2 + S4^2 + $5^2 + $6^2 + $7^2 } END {printf "%.0f", total}' Simulated_Lolium_perenne_RADseq_DEPTH.csv) / $(($(cat Simulated_Lolium_perenne_RADseq_DEPTH.csv | wc -l) * $NPOO)))) - $(($aveDEPTH*$aveDEPTH))))
fi

#OUTPUT TO SUMMARY FILE sim_${4}_${1}_${2}_${3}_${enzyme1}_${enzyme2}_.out --then concatenate everybody into per insert length--- ~/RADseq_SIMULATION.out
# <REP> 	<MIN_FRAG> 	<MAX_FRAG> 	<ENZYME_1> 	<ENZYME_2> 	<N_SCAFF> 	<TOTAL_SNP> <AVE_DEPTH> <VAR_DEPTH>
touch ~/sim_${4}_${1}_${2}_${3}_${enzyme1}_${enzyme2}_.out
echo -e "$4\t$1\t$(($1+$2))\t$3\t$enzyme1\t$enzyme2\t$(cat SCAF.names.temp | wc -l)\t$(paste -sd+ SCAF.counts.temp | bc)\t$aveDEPTH\t$varDEPTH" >> ~/sim_${4}_${1}_${2}_${3}_${enzyme1}_${enzyme2}_.out

#clean up
rm bam.list *.temp *${enzyme1}x${enzyme2}*.*
#mkdir ${enzyme1}x${enzyme2}
#mv *${enzyme1}x${enzyme2}*.* ${enzyme1}x${enzyme2}/
####DELETE RAW SEQUENCES !!!!
#rm ${enzyme1}x${enzyme2}/*.fastq ${enzyme1}x${enzyme2}/*.fasta ${enzyme1}x${enzyme2}/*.sam *.temp

cd ~/SOFTWARES/ddRADseqTools/Package*/
done
done

rm -R /${VOLUME}/SIMULATED/DNA_r${4}

#cp Simulated_Lolium_perenne_RADseq_COVERAGE.csv ~/Simulated_Lolium_perenne_RADseq_COVERAGE-MIN${1}xMAX$(($1+$2))xINSERTLEN${3}xREP${4}.csv
