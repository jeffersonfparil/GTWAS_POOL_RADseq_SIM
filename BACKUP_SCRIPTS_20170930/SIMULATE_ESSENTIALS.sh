#!/bin/bash
##################################
### run essentials only 20170904
### OBJECTIVES of [1] GENOMICS:
###		(1) Which are the best RE pair and fragment length to use in RADseq? --> maximize number of scaffolds and SNPs captured!!!
###		(2) How many reads is best to represent the transcriptome? --> maximize number of aligned transcriptome with the 2017-Shinozuka L. perenne transcriptome
cd ~/SCRIPTS
chmod +x *.py; chmod +x *.sh; chmod +x *.R
./fixREF.py /home/ubuntu/SEQUENCES/DNA/ lope_V1.0.fasta Lolium_perenne_genome_FIXED.fasta
	#ARGUMENTS:
	#	(1) directory where the input fasta file to fix is
	#	(2)	input fasta file to fix
	#	(3) filename of the out_genesput fasta file
	#out_genesPUTS:
	#	Lolium_perenne_genome_FIXED.fasta --> each line truncated just right for bwa and bcftools!
echo -e "REP\tMIN_FRAG\tMAX_FRAG\tINSERT_LEN\tENZYME_1\tENZYME_2\tN_SCAFF\tTOTAL_SNP\tMEAN_DEPTH\tVAR_DEPTH" > ~/RADseq_SIMULATION.out
NIND=500	#NIND=100
VPSC=20		#VPSC=20
NPOO=5		#NPOO=5
NREP=4		#NREP=5
DIR=mnt
for min in $(seq 100 50 700)
do
	for inc in $(seq 50 10 100)
	do
		for insertLen in $(seq $min 10 $(($min + $inc)))
		do
			parallel ./__simulate_RADseq_without_phenotype_simulation__.sh ${min} ${inc} ${insertLen} {1} ${NIND} ${VPSC} ${NPOO} ${DIR} ::: $(seq $NREP)
			cat ~/sim_* >> ~/RADseq_SIMULATION.out
			rm ~/sim_*
		done
	done
done
#ARGUMENTS:
#	{1} --> minimum fragment size (bp)
#	{2} --> increment in fragment size to define maximum fragment size (maximum fragment size = {1} + {2}) (bp)
#	{3} --> insert length (bp)
#	{4} --> replicate
#	{5} --> number of individuals to simulate
#	{6} --> number of variants per scaffold
#	{7} --> number of pools
#	{8} --> home directory i.e. mnt (for newNECTAR and sim03) and volume1 (for sim02)
#OUTPUT:
#	counts into ~/RADseq_SIMULATION.out
# <REP> 	<MIN_FRAG> 	<MAX_FRAG> 	<INSERT_LEN> 	<ENZYME_1> 	<ENZYME_2> 	<N_SCAFF> 	<TOTAL_SNP>